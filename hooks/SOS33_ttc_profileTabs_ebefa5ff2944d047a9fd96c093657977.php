class SOS33_ttc_profileTabs extends skin_profile(~id~)
{

/**
 * Product Title:		(SOS34) Topic Title Colored
 * Product Version:		2.2.0
 * Author:				Adriano Faria
 * Website:				SOS Invision
 * Website URL:			http://forum.sosinvision.com.br/
 * Email:				administracao@sosinvision.com.br
 */

	function tabSingleColumn($row=array(), $read_more_link='', $url='', $title='')
	{
		if ( $this->settings['SOS33_ttc_profiletab'] )
		{
			$style = "";

			if ( $row['ttc_fontcolor'] OR $row['ttc_backgroundcolor'] OR $row['ttc_bold'] OR $row['ttc_italic'] )
			{
				$style .= " style='";

				if ( $row['ttc_backgroundcolor'] )
				{
					$style .= "-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px;padding-left:5px;padding-right:5px;padding-top:2px;padding-bottom:2px;";
				}					

				$style .= ( $row['ttc_fontcolor'] ) ? "color: {$row['ttc_fontcolor']}; " : '';
				$style .= ( $row['ttc_backgroundcolor'] ) ? "background-color: {$row['ttc_backgroundcolor']}; " : '';
				$style .= ( $row['ttc_italic'] ) ? "font-style: italic; " : '';
				$style .= ( $row['ttc_bold'] ) ? "font-weight: bold; " : '';
				$style .= "'";
	
				$title = "<span{$style}>".IPSText::truncate($title,90)."</span>";
			}

			return parent::ttc_tabSingleColumn( $row, $read_more_link, $url, $title );
		}
		else
		{
			return parent::tabSingleColumn( $row, $read_more_link, $url, $title );
		}
	}
}