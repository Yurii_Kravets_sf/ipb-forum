<?php

class SOS33_ttc_newtopic
{

/**
 * Product Title:		(SOS34) Topic Title Colored
 * Product Version:		2.2.0
 * Author:				Adriano Faria
 * Website:				SOS Invision
 * Website URL:			http://forum.sosinvision.com.br/
 * Email:				administracao@sosinvision.com.br
 */

	protected $request;
	
	public function __construct()
	{
		$this->registry  	= ipsRegistry::instance();
		$this->memberData	=& $this->registry->member()->fetchMemberData();
		$this->settings   	=& $this->registry->fetchSettings();
		$this->request    	=& $this->registry->fetchRequest();
	}

	public function handleData( $data )
	{
 		if ( in_array( $this->memberData['member_group_id'], explode( ',', $this->settings['SOS33_ttc_grupos'] ) ) AND in_array( $this->request['f'], explode( ',', $this->settings['SOS33_ttc_forums'] ) ) )
 		{
			$data['ttc_fontcolor']		 = $this->request['ttc_fontcolor'];
			$data['ttc_backgroundcolor'] = $this->request['ttc_backgroundcolor'];
			$data['ttc_bold'] 			 = ( $this->request['ttc_bold'] == 1 ) ? 1: 0;
			$data['ttc_italic'] 		 = ( $this->request['ttc_italic'] == 1 ) ? 1 : 0;
		}
		
		return $data;
	}
}