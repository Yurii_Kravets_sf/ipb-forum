<?php
      
//-----------------------------------------------
// (DP34) Pages
//-----------------------------------------------
//-----------------------------------------------
// Template Hook
//-----------------------------------------------
// Author: DawPi
// Site: http://www.ipslink.pl
// Written on: 29 / 07 / 2011
//-----------------------------------------------
// Copyright (C) 2011 DawPi
// All Rights Reserved
//-----------------------------------------------     

class dp3_cp_globalTemplateTemplateHook
{
	public $registry;	
				
	public function __construct()
	{
		$this->registry   = ipsRegistry::instance();
		$this->DB	    = $this->registry->DB();
		$this->settings =& $this->registry->fetchSettings();
		$this->request  =& $this->registry->fetchRequest();
		$this->member   = $this->registry->member();
		$this->memberData =& $this->registry->member()->fetchMemberData();
		$this->lang		=  $this->registry->getClass('class_localization');
		$this->cache	= $this->registry->cache();
		$this->caches   =& $this->registry->cache()->fetchCaches();	
	}
	
	
	public function getOutput()
	{		
		/* Load lib */
				
		$classToLoad = IPSLib::loadLibrary( IPSLib::getAppDir( 'cp' ) . '/sources/classes/library.php', 'cpLibrary', 'cp' );
		$this->cpLibrary = new $classToLoad( $this->registry );
		
		/* Check access */

		if( ! $this->cpLibrary->checkAccess() )
		{
			return '';		
		}
		
		/* Get pages cache */
		
		$_pages = $this->cache->getCache( 'cp_pages' );
		
		/* Any active page? */
		
        if( is_array( $_pages ) && count( $_pages ) )
        {
    		foreach( $_pages as $page )
    		{
    			if( $page['cp_active'] && $page['cp_show_in_top'] )
    			{
    				if ( in_array( $this->memberData['member_group_id'], explode(',', $page['cp_groups'] ) ) )
    				{
    					$page['cp_title_seo'] 		= IPSText::makeSeoTitle( $page['cp_title'] );
    					$pages[ $page['cp_id'] ] 	= $page; 	
    				}	
    			}
    		}
        }
		
		/* Any page? */
		
		if( ! is_array( $pages ) || ! count( $pages ) )
		{
			return '';
		}
										
		/* Return data */

		return $this->registry->output->getTemplate( 'cp' )->showMenu( $pages );
	}
} //End of class