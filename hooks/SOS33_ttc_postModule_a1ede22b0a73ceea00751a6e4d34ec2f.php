<?php

/**
 * Product Title:		(SOS34) Topic Title Colored
 * Product Version:		2.2.0
 * Author:				Adriano Faria
 * Website:				SOS Invision
 * Website URL:			http://forum.sosinvision.com.br/
 * Email:				administracao@sosinvision.com.br
 */

class SOS33_ttc_postModule extends classPostForms
{
private $formType = '';
	
	public function hook_topicTitleColored()
	{
		if ( ! in_array( $this->formType, array( 'new', 'edit' ) ) )
		{
			return false;
		}
		
		switch( $this->formType )
		{
			case 'new':
				$checkFunc  = 'topicSetUp';
			break;
			case 'edit': 
				$checkFunc  = 'editSetUp';
			break;
		}
		
		//-----------------------------------------
		// Global checks and functions
		//-----------------------------------------
	
		try
		{
			$this->globalSetUp();
		}
		catch( Exception $error )
		{
			$e = $error->getMessage();
			
			if ( $this->formType == 'edit' AND $e == 'NO_POSTING_PPD' )
			{
			
			}
			else
			{
				throw new Exception( $e );
			}
		}
		
		//-----------------------------------------
		// Form specific...
		//-----------------------------------------
		
		try
		{
			$topic = $this->$checkFunc();
		}
		catch( Exception $error )
		{
			throw new Exception( $error->getMessage() );
		}
		
		$styles = array();
		
		foreach( array( 'fontcolor', 'backgroundcolor', 'italic', 'bold' ) as $style )
		{
			$styles['ttc_' . $style ] 	= isset( $this->request['ttc_' . $style ] ) ? $this->request['ttc_' . $style ] : '';
		}
		
		if( $this->_originalPost['new_topic'] )
		{
			foreach( array( 'fontcolor', 'backgroundcolor', 'italic', 'bold' ) as $style )
			{
				if ( ! isset( $styles['ttc_' . $style ] ) OR ! $styles['ttc_' . $style ] )
				{
					if ( isset( $topic['ttc_' . $style ] ) AND $topic['ttc_' . $style ] )
					{
						$styles['ttc_' . $style ] = $topic['ttc_' . $style ];
					}
				}
			}
		}
		
		return $styles;
	}
	
	public function showEditForm()
	{
		$this->formType = 'edit';
		
		$this->cache->updateCacheWithoutSaving( 'topicTitleColored', $this->hook_topicTitleColored() );
		
		parent::showEditForm();
	}
	
	public function showTopicForm()
	{
		$this->formType = 'new';
		
		$this->cache->updateCacheWithoutSaving( 'topicTitleColored', $this->hook_topicTitleColored() );
		
		parent::showTopicForm();
	}
	
	public function editPost()
	{
		try
		{
			$return = parent::editPost();

			if ( $return === TRUE )
			{
				if( $this->_originalPost['new_topic'] == 1 )
				{
					$bold 		= 0;
					$italic		= 0;
					$toUpdate 	= array();

                    if ( isset( $this->request['ttc_fontcolor'] ) and $this->request['ttc_fontcolor'] )
                    {
                        $toUpdate['ttc_fontcolor'] = $this->request['ttc_fontcolor'];
                    }
                    else
                    {
						$toUpdate['ttc_fontcolor'] = '';
					}

                    if ( isset( $this->request['ttc_backgroundcolor'] ) and $this->request['ttc_backgroundcolor'] )
                    {
                        $toUpdate['ttc_backgroundcolor'] = $this->request['ttc_backgroundcolor'];
                    }
                    else
                    {
						$toUpdate['ttc_backgroundcolor'] = '';
					}

                    if ( isset( $this->request['ttc_bold'] ) and $this->request['ttc_bold']  == 1 )
                    {
                        $bold = 1;
                    }
                    else
                    {
						$bold = 0;
					}
					
					$toUpdate['ttc_bold'] 	= $bold;

                    if ( isset( $this->request['ttc_italic'] ) and $this->request['ttc_italic'] == 1 )
                    {
                        $italic = 1;
                    }
                    else
                    {
						$italic = 0;
					}

					$toUpdate['ttc_italic'] = $italic;

					if ( count( $toUpdate ) )
					{
						$classToLoad = IPSLib::loadLibrary( IPSLib::getAppDir( 'forums' ) . "/sources/classes/topics.php", 'app_forums_classes_topics', 'forums' );
						$topicClass = new $classToLoad( $this->registry );

						$topic = $topicClass->getTopicById( $this->request['t'] );
						$topic = array_merge( $topic, $toUpdate );

						$topicClass->updateTopic( $topic['tid'], $toUpdate );
						
						/* Needs this in case someone but the topic starter edits the topic */
						$this->setAuthor( $topic['starter_id'] );
						
						$dbs = array();
						$this->registry->cache()->updateCacheWithoutSaving( 'topicData', $topic );
						
						$this->DB->setDataType( array( 'last_poster_name', 'seo_last_name', 'seo_last_title', 'last_title' ), 'string' );

						/* Data Hook Location */
						IPSLib::doDataHooks( $dbs, 'updateForumLastPostData' );
	
						$this->DB->update( 'forums', $dbs, "id=".intval($topic['forum_id']) );
					}
				}
			}

			return $return;
		}
		catch( Exception $error )
		{
			throw new Exception( $error->getMessage() );
		}
	}
	
	protected function updateForumAndStats( $topic, $type='new')
	{
		$this->registry->cache()->updateCacheWithoutSaving( 'topicData', $topic );
		
		return parent::updateForumAndStats( $topic, $type );
	}
}