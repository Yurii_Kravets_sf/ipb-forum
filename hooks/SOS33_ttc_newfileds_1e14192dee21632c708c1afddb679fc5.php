<?php

/**
 * Product Title:		(SOS34) Topic Title Colored
 * Product Version:		2.2.0
 * Author:				Adriano Faria
 * Website:				SOS Invision
 * Website URL:			http://forum.sosinvision.com.br/
 * Email:				administracao@sosinvision.com.br
 */

class SOS33_ttc_newfileds
{

	public $registry;
	public $DB;
	public $request;
	public $lang;
	
	public function __construct()
	{
		$this->registry  	= ipsRegistry::instance();
		$this->memberData	=& $this->registry->member()->fetchMemberData();
		$this->settings   	=& $this->registry->fetchSettings();
		$this->request    	=& $this->registry->fetchRequest();
		$this->DB         	=  $this->registry->DB();
		$this->cache    	=  $this->registry->cache();
		$this->caches   	=& $this->registry->cache()->fetchCaches();
	}
	
	public function getOutput()
	{
		if ( $this->request['do'] == 'reply_post' )
		{
			return false;
		}

 		if ( !in_array( $this->memberData['member_group_id'], explode( ',', $this->settings['SOS33_ttc_grupos'] ) ) OR !in_array( $this->request['f'], explode( ',', $this->settings['SOS33_ttc_forums'] ) ) )
 		{
			return false;
		}

		return $this->registry->getClass('output')->getTemplate( 'post' )->topicTitleColored( $this->caches['topicTitleColored'] );	
	}
}