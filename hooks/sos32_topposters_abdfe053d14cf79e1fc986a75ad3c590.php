<?php

class sos32_topposters
{
	public $registry;
	public $member;
	public $cache;
	
	public function __construct()
	{
		$this->registry = ipsRegistry::instance();
		$this->DB		=  $this->registry->DB();
		$this->settings =& $this->registry->fetchSettings();
		$this->memberData =& $this->registry->member()->fetchMemberData();
		$this->cache    = $this->registry->cache();
		$this->caches   =& $this->registry->cache()->fetchCaches();
		$this->registry->class_localization->loadLanguageFile( array( 'public_boards', 'forums' ) );
	}

	public function getOutput()
	{
		if( !in_array( $this->memberData['member_group_id'], explode( ',', $this->settings['sos32_topposters_groups'] ) ) )
           {
           	return false;
		}

		$qtd = $this->settings['sos32_topposters_qtd'] ? $this->settings['sos32_topposters_qtd'] : 5;

		$banned = !$this->settings['sos32_topposters_banned'] ? "AND m.member_group_id <> ".$this->settings['banned_group']." AND m.member_banned = 0" : "";

		$this->DB->build( array( 
                                                                'select' => 'm.member_id, m.members_seo_name, m.members_display_name, m.member_group_id, m.posts',
                                                                'from'   => array( 'members' => 'm' ),
                                                'add_join' => array(
                                                                0 => array( 'select' => 'pp.*',
                                                                                         'from'   => array( 'profile_portal' => 'pp' ),
                                                                                         'where'  => 'pp.pp_member_id=m.member_id',
                                                                                         'type'   => 'left' )
                                        ),
                                        'where' => "m.posts > 0 ".$banned,
'order' => 'm.posts DESC',
                                                                'limit' => array( 0, $qtd )
                ) );
                                                                 
		$q = $this->DB->execute();
                        
		if ( $this->DB->getTotalRows( $q ) )
		{
			$member_ids = array();
	
			while ( $r = $this->DB->fetch( $outer ) )
	        {
				$member_ids[ $r['member_id'] ] = $r['member_id'];
			}
	
			if( count($member_ids) )
			{
				$users		= array();
				$members	= IPSMember::load( $member_ids, 'all' );
	
				foreach( $members as $mid => $member )
				{
					$member = IPSMember::buildDisplayData( $member );
					$users[ $mid ]  = $member;
				}
	
				uasort( $users, array( $this, 'postSort' ) );
			}
	
			return $this->registry->output->getTemplate( 'boards' )->boardIndexTopPosters( $users );
        }
	}

	private function postSort( $a, $b )
	{
		if ( $a['posts'] == $b['posts'] )
		{
			return 0;
		}

		return ( $a['posts'] > $b['posts'] ) ? -1 : 1;
	}
}