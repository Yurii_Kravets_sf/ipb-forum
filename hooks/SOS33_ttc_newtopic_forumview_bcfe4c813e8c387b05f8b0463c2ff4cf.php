<?php

/**
 * Product Title:		(SOS34) Topic Title Colored
 * Product Version:		2.2.0
 * Author:				Adriano Faria
 * Website:				SOS Invision
 * Website URL:			http://forum.sosinvision.com.br/
 * Email:				administracao@sosinvision.com.br
 */
 
class SOS33_ttc_newtopic_forumview
{
	protected $registry;
	protected $DB;
	protected $request;
	protected $lang;
	protected $prefixes;
	
	public function __construct()
	{
		$this->registry		= ipsRegistry::instance();
		$this->DB			= $this->registry->DB();
		$this->request  	=& $this->registry->fetchRequest();
		$this->lang			= $this->registry->getClass('class_localization');
	}
	
	public function getOutput()
	{
		$data		= $this->registry->output->getTemplate('forum')->functionData['forumIndexTemplate'][0];
		$style		= "";
		
		foreach( $data['topic_data'] as $topic )
		{
			if ( ! $topic['ttc_fontcolor'] AND ! $topic['ttc_backgroundcolor'] AND ! $topic['ttc_bold'] AND ! $topic['ttc_italic'] )
			{
				continue;
			}
			
			$style .= "#tid-link-{$topic['tid']} {" . PHP_EOL;

			if ( $topic['ttc_backgroundcolor'] )
			{
				$style .= "-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px;padding-left:5px;padding-right:5px;padding-top:2px;padding-bottom:2px;";
			}
			
			$style .= ( $topic['ttc_fontcolor'] ) ? "\tcolor: {$topic['ttc_fontcolor']};" . PHP_EOL : '';
			$style .= ( $topic['ttc_backgroundcolor'] ) ? "\tbackground-color: {$topic['ttc_backgroundcolor']};" . PHP_EOL : '';
			$style .= ( $topic['ttc_italic'] ) ? "\tfont-style: italic;" . PHP_EOL : '';
			$style .= ( $topic['ttc_bold'] ) ? "\tfont-weight: bold;" . PHP_EOL : '';
			$style .= "}" . PHP_EOL;
		}
		
		return ( ! empty( $style ) ) ? "<style type='text/css'>{$style}</style>" : '';
	}
	
	/*public function replaceOutput( $output, $key )
	{
		$data		= $this->registry->output->getTemplate('forum')->functionData['forumIndexTemplate'][0];
		$tag		= '<!--hook.'.$key.'-->';
		$lastFound	= 0;
		
		$fc = "";
		$bg = "";

		foreach( $data['topic_data'] as $tid => $topic )
		{
			$tag2 = '"topic-'.$tid.'"';
			
			$pos = strpos( $output, $tag, $lastFound );
			$pos = $pos ? $pos : strpos( $output, '"topic-'.$tid.'"', $lastFound ) + strlen( $tag2 ) + 1;

			if ( $topic['ttc_fontcolor'] )
			{
				$fc = 'color:'.$topic['ttc_fontcolor'].';';
			}

			if ( $topic['ttc_backgroundcolor'] )
			{
				$bg = 'background-color:'.$topic['ttc_backgroundcolor'].';';
			}
			
			if ( $topic['ttc_bold'] )
			{
				$bold = 'font-weight:bold;';
			}

			if ( $topic['ttc_italic'] )
			{
				$italic = 'font-style: italic;';
			}
			
			if ( $fc OR $bg OR $bold or $italic )
			{
				$strToInsert = '<div style="float:left;'.$fc.$bg.$bold.$italic.'">';
			}
			
			if( $pos ) {
				$output = substr_replace( $output, $strToInsert, $pos, 0 );
				$lastFound = $pos + strlen( $tag . $strToInsert );
			}
		}
				
		return $output;
	}*/
}
