<?php

class SOS33_ttc_search
{	

/**
 * Product Title:		(SOS34) Topic Title Colored
 * Product Version:		2.2.0
 * Author:				Adriano Faria
 * Website:				SOS Invision
 * Website URL:			http://forum.sosinvision.com.br/
 * Email:				administracao@sosinvision.com.br
 */
 
	public function __construct()
	{
		$this->registry		= ipsRegistry::instance();
		$this->DB			= $this->registry->DB();
		$this->request  	=& $this->registry->fetchRequest();
		$this->lang			= $this->registry->getClass('class_localization');
		$this->cache      =  $this->registry->cache();
		$this->caches     =& $this->registry->cache()->fetchCaches();
		$this->settings   	=& $this->registry->fetchSettings();
	}
	
	public function getOutput()
	{
		$data			= $this->caches['resultSet'];
		$selectorTitle 	= $this->lang->words['view_result'];
		$style			= "";
	
		if ( is_array( $data ) AND count( $data ) )
		{
			foreach( $data as $topic )
			{
				if ( ! $topic['ttc_fontcolor'] AND ! $topic['ttc_backgroundcolor'] AND ! $topic['ttc_bold'] AND ! $topic['ttc_italic'] )
				{
					continue;
				}
				
				if ( !$this->settings['SOS33_ttc_searchresults'] )
				{
					continue;
				}
				
				$style .= "#trow_{$topic['tid']} a[title='{$selectorTitle}'] {" . PHP_EOL;

				if ( $topic['ttc_backgroundcolor'] )
				{
					$style .= "-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px;padding-left:5px;padding-right:5px;padding-top:2px;padding-bottom:2px;";
				}

				$style .= ( $topic['ttc_fontcolor'] ) ? "\tcolor: {$topic['ttc_fontcolor']};" . PHP_EOL : '';
				$style .= ( $topic['ttc_backgroundcolor'] ) ? "\tbackground-color: {$topic['ttc_backgroundcolor']};" . PHP_EOL : '';
				$style .= ( $topic['ttc_italic'] ) ? "\tfont-style: italic;" . PHP_EOL : '';
				$style .= ( $topic['ttc_bold'] ) ? "\tfont-weight: bold;" . PHP_EOL : '';
				$style .= "}" . PHP_EOL;
			}
		}
		
		return ( ! empty( $style ) ) ? "<style type='text/css'>{$style}</style>" : '';
	}
}