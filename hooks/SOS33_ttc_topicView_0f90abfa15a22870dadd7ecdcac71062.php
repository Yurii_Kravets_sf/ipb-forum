<?php

/**
 * Product Title:		(SOS34) Topic Title Colored
 * Product Version:		2.2.0
 * Author:				Adriano Faria
 * Website:				SOS Invision
 * Website URL:			http://forum.sosinvision.com.br/
 * Email:				administracao@sosinvision.com.br
 */

class SOS33_ttc_topicView
{
	public function __construct()
	{
		$this->registry		= ipsRegistry::instance();
		$this->DB			= $this->registry->DB();
		$this->request  	=& $this->registry->fetchRequest();
		$this->lang			= $this->registry->getClass('class_localization');
	}
	
	public function getOutput()
	{
		$topic		= $this->registry->output->getTemplate('topic')->functionData['topicViewTemplate'][0]['topic'];
		$style		= "";

		if ( $topic['ttc_fontcolor'] OR $topic['ttc_backgroundcolor'] OR $topic['ttc_bold'] OR $topic['ttc_italic'] )
		{	
			$style .= ".ipsType_pagetitle {" . PHP_EOL;
			
			if ( $topic['ttc_backgroundcolor'] )
			{
				$style .= "-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px;padding-left:5px;padding-right:5px;padding-top:2px;padding-bottom:2px;display: inline;";
			}

			$style .= ( $topic['ttc_fontcolor'] ) ? "\tcolor: {$topic['ttc_fontcolor']};" . PHP_EOL : '';
			$style .= ( $topic['ttc_backgroundcolor'] ) ? "\tbackground-color: {$topic['ttc_backgroundcolor']};" . PHP_EOL : '';
			$style .= ( $topic['ttc_italic'] ) ? "\tfont-style: italic;" . PHP_EOL : '';
			$style .= ( $topic['ttc_bold'] ) ? "\tfont-weight: bold;" . PHP_EOL : '';
			$style .= "}" . PHP_EOL;
		}
		
		return ( ! empty( $style ) ) ? "<style type='text/css'>{$style}</style>" : '';
	}
}