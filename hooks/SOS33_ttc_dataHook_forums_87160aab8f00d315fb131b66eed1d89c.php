<?php

class SOS33_ttc_dataHook_forums
{

/**
 * Product Title:		(SOS34) Topic Title Colored
 * Product Version:		2.2.0
 * Author:				Adriano Faria
 * Website:				SOS Invision
 * Website URL:			http://forum.sosinvision.com.br/
 * Email:				administracao@sosinvision.com.br
 */

	public function handleData( $forum_data )
	{
		$topic = ipsRegistry::instance()->cache()->getCache( 'topicData' );
		$styleArray = array();
		
		if ( $topic['ttc_fontcolor'] OR $topic['ttc_backgroundcolor'] OR $topic['ttc_bold'] OR $topic['ttc_italic'] )
		{
			$styleArray = array(
								'styleForTid'			=> $topic['tid'],
								'ttc_fontcolor' 		=> $topic['ttc_fontcolor'],
								'ttc_backgroundcolor' 	=> $topic['ttc_backgroundcolor'],
								'ttc_bold'				=> $topic['ttc_bold'],
								'ttc_italic'			=> $topic['ttc_italic'],
			);
			
		}
		
		$forum_data['last_title_style'] = serialize( $styleArray );
		
		return $forum_data;
	}
}