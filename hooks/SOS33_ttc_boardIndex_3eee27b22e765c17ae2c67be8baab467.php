<?php

/**
 * Product Title:		(SOS34) Topic Title Colored
 * Product Version:		2.2.0
 * Author:				Adriano Faria
 * Website:				SOS Invision
 * Website URL:			http://forum.sosinvision.com.br/
 * Email:				administracao@sosinvision.com.br
 */

class SOS33_ttc_boardIndex extends class_forums
{
	public function forumsFormatLastinfo( $forum_data )
	{
		if ( !$this->settings['SOS33_ttc_boardindex'] )
		{
			return parent::forumsFormatLastInfo( $forum_data );
		}

		$forum_data = parent::forumsFormatLastInfo( $forum_data );
		
		if ( $forum_data['last_topic_title'] AND $forum_data['_hide_last_date'] != TRUE )
		{
			$topic = unserialize( $forum_data['last_title_style'] );
			
			if ( ( ! $topic['styleForTid'] OR $topic['styleForTid'] == $forum_data['last_id'] ) AND ( $topic['ttc_fontcolor'] OR $topic['ttc_backgroundcolor'] OR $topic['ttc_bold'] OR $topic['ttc_italic'] ) )
			{
				$style .= " style='";
				$style .= "-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px;padding-left:5px;padding-right:5px;padding-top:2px;padding-bottom:2px;";
				$style .= ( $topic['ttc_fontcolor'] ) ? "color: {$topic['ttc_fontcolor']}; " : '';
				$style .= ( $topic['ttc_backgroundcolor'] ) ? "background-color: {$topic['ttc_backgroundcolor']}; " : '';
				$style .= ( $topic['ttc_italic'] ) ? "font-style: italic; " : '';
				$style .= ( $topic['ttc_bold'] ) ? "font-weight: bold; " : '';
				$style .= "'";
				
				$forum_data['last_topic_title']  = "<a{$style} href='" . $this->registry->getClass('output')->formatUrl( $this->registry->getClass('output')->buildUrl( "showtopic={$forum_data['last_id']}&amp;view=getnewpost", 'public' ), $forum_data['seo_last_title'], 'showtopic' ) . "' title='" . $this->lang->words['tt_gounread'] . ": {$forum_data['full_last_title']}'>{$forum_data['last_title']}</a>";
		
			}
		}

		return $forum_data;
	}
	
	public function forumsCalcChildren( $root_id, $forum_data=array(), $done_pass=0 )
	{
		//-----------------------------------------
		// Markers
		//-----------------------------------------

		$rtime = $this->registry->classItemMarking->fetchTimeLastMarked( array( 'forumID' => $forum_data['id'] ), 'forums' );

		if( !isset($forum_data['_has_unread']) )
		{
			$forum_data['_has_unread'] = ( $forum_data['last_post'] && $forum_data['last_post'] > $rtime ) ? 1 : 0;
		}

		if ( isset( $this->forum_cache[ $root_id ]) AND is_array( $this->forum_cache[ $root_id ] ) )
		{
			foreach( $this->forum_cache[ $root_id ] as $data )
			{
				if ( $data['last_post'] > $forum_data['last_post'] AND ! $data['hide_last_info'] )
				{
					$forum_data['last_post']			= $data['last_post'];
					$forum_data['fid']					= $data['id'];
					$forum_data['last_id']				= $data['last_id'];
					$forum_data['last_title']			= $data['last_title'];
					$forum_data['seo_last_title']		= $data['seo_last_title'];
					$forum_data['password']				= isset( $data['password'] ) ? $data['password'] : '';
					$forum_data['password_override']	= isset( $data['password_override'] ) ? $data['password_override'] : '';
					$forum_data['last_poster_id']		= $data['last_poster_id'];
					$forum_data['last_poster_name']		= $data['last_poster_name'];
					$forum_data['seo_last_name']		= $data['seo_last_name'];
					$forum_data['_has_unread']          = $forum_data['_has_unread'];
					$forum_data['last_title_style']		= $data['last_title_style']; // TTC
				}
				
				//-----------------------------------------
				// Markers.  We never set false from inside loop.
				//-----------------------------------------
				
				$rtime	             = $this->registry->classItemMarking->fetchTimeLastMarked( array( 'forumID' => $data['id'] ), 'forums' );
				$data['_has_unread'] = 0;
				
				if( $data['last_post'] && $data['last_post'] > $rtime )
				{
					$forum_data['_has_unread']		= 1;
					$data['_has_unread']            = 1;
				}

				//-----------------------------------------
				// Topics and posts
				//-----------------------------------------
				
				$forum_data['posts']  += $data['posts'];
				$forum_data['topics'] += $data['topics'];
				
				$_mod = ( isset( $this->_memberData['forumsModeratorData'] ) ) ? $this->_memberData['forumsModeratorData'] : array();
				
				if ( $this->_memberData['g_is_supmod'] or ( $_mod && !empty( $_mod[ $data['id'] ]['post_q'] ) ) )
				{
					$forum_data['queued_posts']  += $data['queued_posts'];
					$forum_data['queued_topics'] += $data['queued_topics'];
				}
				
				if ( ! $done_pass )
				{
					$forum_data['subforums'][ $data['id'] ] = array($data['id'], $data['name'], $data['name_seo'], intval( $data['_has_unread']  ), 0 );
				}
				
				$forum_data = $this->forumsCalcChildren( $data['id'], $forum_data, 1 );
			}
		}

		return $forum_data;
	}
	
	public function hooks_recentTopics( $topicCount=5, $output=true )
	{
		/* INIT */
		$topicIDs	= array();
		$topic_rows = array();
		$timesUsed	= array();
		$bvnp       = explode( ',', $this->settings['vnp_block_forums'] );
		
		$this->registry->class_localization->loadLanguageFile( array( 'public_topic' ), 'forums' );
		
		/* Grab last X data */
		foreach( $this->forum_by_id as $forumID => $forumData )
		{
			if ( ! $forumData['can_view_others'] )
			{
				continue;
			}
			
			if ( $forumData['password'] )
			{
				continue;
			}
			
			if ( ! $this->registry->permissions->check( 'read', $forumData ) )
			{
				continue;
			}
			
			if ( is_array( $bvnp ) AND count( $bvnp ) )
			{
				if ( in_array( $forumID, $bvnp ) )
				{
					continue;
				}
			}
			
			if ( $this->settings['forum_trash_can_id'] AND $forumID == $this->settings['forum_trash_can_id'] )
			{
				continue;
			}
			
			/* Still here? */
			$_topics = $this->lastXThaw( $forumData['last_x_topic_ids'] );
			
			if ( is_array( $_topics ) )
			{
				foreach( $_topics as $id => $time )
				{
					if( in_array( $time, $timesUsed ) )
					{
						while( in_array( $time, $timesUsed ) )
						{
							$time +=1;
						}
					}
					
					$timesUsed[]       = $time;
					$topicIDs[ $time ] = $id;
				}
			}
		}
		
		$timesUsed	= array();
		
		if ( is_array( $topicIDs ) && count( $topicIDs ) )
		{
			krsort( $topicIDs );
			
			/* We get up to double in case some of the latest are moved_to links - we do another array_slice afterwards to limit to right limit */
			$_topics	= array_slice( $topicIDs, 0, $topicCount * 2 );
			
			if ( is_array( $_topics ) && count( $_topics ) )
			{
				/* Query Topics */
				$this->registry->DB()->build( array( 
														'select'   => 't.tid, t.title as topic_title, t.title_seo, t.start_date, t.starter_id, t.starter_name, t.moved_to, t.views, t.posts, t.ttc_fontcolor, t.ttc_backgroundcolor, t.ttc_bold, t.ttc_italic',
														'from'     => array( 'topics' => 't' ),
														'where'    => 't.tid IN (' . implode( ',', array_values( $_topics ) ) . ')',
														'add_join' => array(
																			array(
																					'select'	=> 'm.*',
																					'from'		=> array( 'members' => 'm' ),
																					'where'		=> 'm.member_id=t.starter_id',
																					'type'		=> 'left',
																				),
																			array(
																					'select'	=> 'pp.*',
																					'from'		=> array( 'profile_portal' => 'pp' ),
																					'where'		=> 'm.member_id=pp.pp_member_id',
																					'type'		=> 'left',
																				),
																		)
											)	 );

				$this->registry->DB()->execute();

				while( $r = $this->registry->DB()->fetch() )
				{
					if( !is_null($r['moved_to']) )
					{
						continue;
					}

					$time	= $r['start_date'];
					
					if( in_array( $time, $timesUsed ) )
					{
						while( in_array( $time, $timesUsed ) )
						{
							$time +=1;
						}
					}
					
					if ( $this->settings['SOS33_ttc_sidebar'] )
					{
						$style = "";
						if ( $output AND ( $r['ttc_fontcolor'] OR $r['ttc_backgroundcolor'] OR $r['ttc_bold'] OR $r['ttc_italic'] ) )
						{
							$style .= " style='";
							
							if ( $r['ttc_backgroundcolor'] )
							{
								$style .= "-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px;padding-left:5px;padding-right:5px;padding-top:2px;padding-bottom:2px;";
							}					

							$style .= ( $r['ttc_fontcolor'] ) ? "color: {$r['ttc_fontcolor']}; " : '';
							$style .= ( $r['ttc_backgroundcolor'] ) ? "background-color: {$r['ttc_backgroundcolor']}; " : '';
							$style .= ( $r['ttc_italic'] ) ? "font-style: italic; " : '';
							$style .= ( $r['ttc_bold'] ) ? "font-weight: bold; " : '';
							$style .= "'";
							
							$r['topic_title'] = "<span{$style}>{$r['topic_title']}</span>";
						}
					}
					
					$timesUsed[]          = $time;
					$topics_rows[ $time ] = IPSMember::buildDisplayData( $r );
				}
				
				/* Got any results? */
				if ( count($topics_rows) )
				{
					krsort( $topics_rows );
					$topics_rows = array_slice( $topics_rows, 0, $topicCount );
				}
			}
		}
		
		if( $output )
		{
			return $this->registry->output->getTemplate( 'boards' )->hookRecentTopics( $topics_rows );
		}
		else
		{
			return $topics_rows;
		}
	}
}