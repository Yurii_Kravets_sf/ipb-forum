<?php

/**
 * Product Title:		(SOS34) Topic Title Colored
 * Product Version:		2.2.0
 * Author:				Adriano Faria
 * Website:				SOS Invision
 * Website URL:			http://forum.sosinvision.com.br/
 * Email:				administracao@sosinvision.com.br
 */

class SOS33_ttc_modLibrary extends moderatorLibrary
{
	/**
	 * Recount a forum
	 *
	 * @return	boolean
	 */
	public function forumRecount( $fid="" )
	{
		$fid = intval($fid);
		
		if ( ! $fid )
		{
			if ( $this->forum['id'] )
			{
				$fid = $this->forum['id'];
			}
			else
			{
				return false;
			}
		}
		
		//-----------------------------------------
		// Get the topics..
		//-----------------------------------------
		
		$topics			= $this->DB->buildAndFetch( array( 'select'	=> 'COUNT(*) as count',
														   'from'	=> 'topics',
														   'where'	=> "approved=1 and forum_id={$fid}" ) );
		
		//-----------------------------------------
		// Get the QUEUED topics..
		//-----------------------------------------
		
		$queued_topics	= $this->DB->buildAndFetch( array( 'select'	=> 'COUNT(*) as count',
																	'from'	=> 'topics',
																	'where'	=> "approved=0 and forum_id={$fid}" ) );
																	
		//-----------------------------------------
		// Get the DELETED topics..
		//-----------------------------------------
		
		$deleted_topics	= $this->DB->buildAndFetch( array( 'select'	=> 'COUNT(*) as count',
														   'from'	=> 'topics',
														   'where'	=> "approved=-1 and forum_id={$fid}" ) );
		
		//-----------------------------------------
		// Get the posts..
		//-----------------------------------------
		
		$posts			= $this->DB->buildAndFetch( array( 'select'	=> 'SUM(posts) as replies',
																	'from'	=> 'topics',
																	'where'	=> "approved=1 and forum_id={$fid}" ) );
		
		//-----------------------------------------
		// Get the QUEUED posts..
		//-----------------------------------------
		
		$queued_posts	= $this->DB->buildAndFetch( array( 'select'	=> 'SUM(topic_queuedposts) as replies',
																	'from'	=> 'topics',
																	'where'	=> "forum_id={$fid}" ) );
		
		//-----------------------------------------
		// Get the DELETED posts..
		//-----------------------------------------
												
		$deleted_posts	= $this->DB->buildAndFetch( array( 'select'	=> 'SUM(topic_deleted_posts) as replies',
																	'from'	=> 'topics',
																	'where'	=> "forum_id={$fid}" ) );
		
		//-----------------------------------------
		// Get the forum last poster..
		//-----------------------------------------
		
		$last_post		= $this->DB->buildAndFetch( array( 'select'	=> 'tid, title, last_poster_id, last_poster_name, seo_last_name, last_post',
																	'from'	=> 'topics',
																	'where'	=> "approved=1 and forum_id={$fid}",
																	'order'	=> 'last_post DESC',
																	'limit'	=> array( 1 ) 
														)		);
		
		$newest_topic	= $this->DB->buildAndFetch( array( 'select'	=> 'title, tid, seo_first_name, ttc_fontcolor, ttc_backgroundcolor, ttc_bold, ttc_italic',
																	'from'	=> 'topics',
																	'where'	=> 'forum_id=' . $fid . ' and approved=1',
																	'order'	=> 'start_date desc',
																	'limit'	=> array( 1 )
															)	  );
		
		$styleArray = array();
		
		if ( $newest_topic['ttc_fontcolor'] OR $newest_topic['ttc_backgroundcolor'] OR $newest_topic['ttc_bold'] OR $newest_topic['ttc_italic'] )
		{
			$styleArray = array(
								'styleForTid'			=> $newest_topic['tid'],
								'ttc_fontcolor' 		=> $newest_topic['ttc_fontcolor'],
								'ttc_backgroundcolor' 	=> $newest_topic['ttc_backgroundcolor'],
								'ttc_bold'				=> $newest_topic['ttc_bold'],
								'ttc_italic'			=> $newest_topic['ttc_italic'],
			);
			
		}
		
		$lastXTopics    = $this->registry->class_forums->lastXFreeze( $this->registry->class_forums->buildLastXTopicIds( $fid, FALSE ) );
		
		//-----------------------------------------
		// Reset this forums stats
		//-----------------------------------------
		
		$dbs = array(
					  'name_seo'			=> IPSText::makeSeoTitle( $this->registry->class_forums->allForums[ $fid ]['name'] ),
					  'last_poster_id'		=> intval($last_post['last_poster_id']),
					  'last_poster_name'	=> $last_post['last_poster_name'],
					  'seo_last_name'       => IPSText::makeSeoTitle( $last_post['last_poster_name'] ),
					  'last_post'			=> intval($last_post['last_post']),
					  'last_title'			=> $last_post['title'],
					  'seo_last_title'      => IPSText::makeSeoTitle( $last_post['title'] ),
					  'last_id'				=> intval($last_post['tid']),
					  'topics'				=> intval($topics['count']),
					  'posts'				=> intval($posts['replies']),
					  'queued_topics'		=> intval($queued_topics['count']),
					  'queued_posts'		=> intval($queued_posts['replies']),
					  'deleted_posts'		=> intval($deleted_posts['replies']),
					  'deleted_topics'		=> intval($deleted_topics['count']),
					  'newest_id'			=> intval($newest_topic['tid']),
					  'newest_title'		=> $newest_topic['title'],
					  'last_x_topic_ids'    => $lastXTopics,
					  'last_title_style'	=> serialize( $styleArray ),
					);
					
		if ( $this->registry->class_forums->allForums[ $fid ]['_update_deletion'] )
		{
			$dbs['forum_last_deletion'] = time();
		}
		
		$this->DB->force_data_type = array( 'last_poster_name'	=> 'string',
											'last_title'		=> 'string',
											'newest_title'		=> 'string',
											'seo_last_title'    => 'string',
											'seo_last_name'     => 'string'  );		
												 
		$this->DB->update( 'forums', $dbs, "id=" . $fid );

		return true;
	}
}