

/**
 * Product Title:		(SOS34) Topic Title Colored
 * Product Version:		2.2.0
 * Author:				Adriano Faria
 * Website:				SOS Invision
 * Website URL:			http://forum.sosinvision.com.br/
 * Email:				administracao@sosinvision.com.br
 */

/* This is just a dummy hook to get access to the data I need in a different templatehook */

class SOS33_ttc_search_dummy extends skin_search(~id~)
{
	function topicPostSearchResultAsForum( $data, $resultAsTitle=false )
	{
		$cache 	= $this->caches['resultSet'];
		
		$cache[ $data['tid'] ] = $data;
		$this->cache->updateCacheWithoutSaving( 'resultSet', $cache );
		
		return parent::topicPostSearchResultAsForum( $data, $resultAsTitle );
	}
}