<?php
/**
 * Global Caches cache. Do not attempt to modify this file.
 * Please modify the relevant setting for each application or hook
 *
 * Written: Thu, 06 Apr 2017 09:27:45 +0000
 *
 * Why? Because Tera says so this time :P
 */

$GLOBAL_CACHES = array (
  0 => 'cp_pages',
);
