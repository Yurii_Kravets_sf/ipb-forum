<?php
/*--------------------------------------------------*/
/* FILE GENERATED BY INVISION POWER BOARD 3         */
/* CACHE FILE: Skin set id: 3               */
/* CACHE FILE: Generated: Thu, 16 Mar 2017 13:54:26 GMT */
/* DO NOT EDIT DIRECTLY - THE CHANGES WILL NOT BE   */
/* WRITTEN TO THE DATABASE AUTOMATICALLY            */
/*--------------------------------------------------*/

class skin_modcp_3 extends skinMaster{

/**
* Construct
*/
function __construct( ipsRegistry $registry )
{
	parent::__construct( $registry );
	

$this->_funcHooks = array();
$this->_funcHooks['deletedPosts'] = array('post_data','disablelightbox','hasPosts');
$this->_funcHooks['deletedTopics'] = array('hastopics');
$this->_funcHooks['editUserForm'] = array('editusercfielddesc','custom_fields','displaynamehistory','modPreview','postingRestricted','suspended','hasRestrictions','groupCanStatus','editusercfields');
$this->_funcHooks['latestWarnLogs'] = array('warningloop','haswarnings');
$this->_funcHooks['membersList'] = array('weAreSupmod','memberwarn','sendpm','blog','gallery','norep','posrep','negrep','repson','isnotbanned','isnotbanned2','members','showmembers');
$this->_funcHooks['membersModIPForm'] = array('modIpIe');
$this->_funcHooks['membersModIPFormMembers'] = array('ipMembers','modIPMembers');
$this->_funcHooks['membersModIPFormPosts'] = array('ipPosts','modFindMemberPosts');
$this->_funcHooks['modAnnounceForm'] = array('buttonlang','announceMessage','buttonlang');
$this->_funcHooks['modAnnouncements'] = array('announce_forums','notactive','notactive','announceHasForums','announceForum','announcements','hasAnnouncements');
$this->_funcHooks['modcpMessage'] = array('hasMessage');
$this->_funcHooks['modCPpost'] = array('reputation','noRep','posRep','negRep','repIgnored','postMid','postMember','postAdmin','postIp','postIsHardDeleted','canSoftDelete','canHardDelete','hasDeletedReasonRow','hasDeletedReason','showReason','postDeletedReason','isDeleted','canEdit','approveUnapprove','approvePost','canDelete','reportedPostData','isUnapproved','postEditByReason','postEditBy','reportedPostData');
$this->_funcHooks['modCPtopic'] = array('haslastpage','pages','isLink','isLinkEnd','multipages','hasStarterId','isntLink','hasDeletedReason','showReason','topicDeletedReason','replylang','isntLink3','tidRestoreLink','canHardDeleteLinkFromDb','topicIsHardDeleted','tidRestore','canHardDeleteFromDb','isntLink4','canSoftDelete','canHardDelete','topics');
$this->_funcHooks['overview'] = array('canEditMember');
$this->_funcHooks['portalPage'] = array('tabs','accessRC');
$this->_funcHooks['subTabLoop'] = array('subTabs','isMainTab','findTab');
$this->_funcHooks['unapprovedPosts'] = array('post_data','disablelightbox','hasPosts');
$this->_funcHooks['unapprovedTopics'] = array('hastopics');


}

/* -- deletedPosts --*/
function deletedPosts($posts, $other_data=array(), $pagelinks='') {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['deletedPosts'] ) )
{
$count_d5ed4d0113783064ae74858204d84ab0 = is_array($this->functionData['deletedPosts']) ? count($this->functionData['deletedPosts']) : 0;
$this->functionData['deletedPosts'][$count_d5ed4d0113783064ae74858204d84ab0]['posts'] = $posts;
$this->functionData['deletedPosts'][$count_d5ed4d0113783064ae74858204d84ab0]['other_data'] = $other_data;
$this->functionData['deletedPosts'][$count_d5ed4d0113783064ae74858204d84ab0]['pagelinks'] = $pagelinks;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- deletedTopics --*/
function deletedTopics($topics, $sdelete=array(), $pagelinks='') {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['deletedTopics'] ) )
{
$count_dacdeeb8be33ee1c1e5419d00ae255dc = is_array($this->functionData['deletedTopics']) ? count($this->functionData['deletedTopics']) : 0;
$this->functionData['deletedTopics'][$count_dacdeeb8be33ee1c1e5419d00ae255dc]['topics'] = $topics;
$this->functionData['deletedTopics'][$count_dacdeeb8be33ee1c1e5419d00ae255dc]['sdelete'] = $sdelete;
$this->functionData['deletedTopics'][$count_dacdeeb8be33ee1c1e5419d00ae255dc]['pagelinks'] = $pagelinks;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- editUserForm --*/
function editUserForm($profile, $custom_fields=null) {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['editUserForm'] ) )
{
$count_c8a82536d3a3e9b5752ca4fc11e2b02b = is_array($this->functionData['editUserForm']) ? count($this->functionData['editUserForm']) : 0;
$this->functionData['editUserForm'][$count_c8a82536d3a3e9b5752ca4fc11e2b02b]['profile'] = $profile;
$this->functionData['editUserForm'][$count_c8a82536d3a3e9b5752ca4fc11e2b02b]['custom_fields'] = $custom_fields;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- inlineModIPMessage --*/
function inlineModIPMessage($msg='') {
$IPBHTML = "";
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- latestWarnLogs --*/
function latestWarnLogs($warnings, $pagelinks) {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['latestWarnLogs'] ) )
{
$count_b8990252cf728c122265c0e8f8ac4e2e = is_array($this->functionData['latestWarnLogs']) ? count($this->functionData['latestWarnLogs']) : 0;
$this->functionData['latestWarnLogs'][$count_b8990252cf728c122265c0e8f8ac4e2e]['warnings'] = $warnings;
$this->functionData['latestWarnLogs'][$count_b8990252cf728c122265c0e8f8ac4e2e]['pagelinks'] = $pagelinks;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- memberLookup --*/
function memberLookup() {
$IPBHTML = "";
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- membersList --*/
function membersList($type, $members, $pagelinks='') {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['membersList'] ) )
{
$count_4402bd5f1990149f9def7981eac15811 = is_array($this->functionData['membersList']) ? count($this->functionData['membersList']) : 0;
$this->functionData['membersList'][$count_4402bd5f1990149f9def7981eac15811]['type'] = $type;
$this->functionData['membersList'][$count_4402bd5f1990149f9def7981eac15811]['members'] = $members;
$this->functionData['membersList'][$count_4402bd5f1990149f9def7981eac15811]['pagelinks'] = $pagelinks;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- membersModIPForm --*/
function membersModIPForm($ip="", $inlineMsg='') {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['membersModIPForm'] ) )
{
$count_d973849250c7c1128b70a013e32e7ab3 = is_array($this->functionData['membersModIPForm']) ? count($this->functionData['membersModIPForm']) : 0;
$this->functionData['membersModIPForm'][$count_d973849250c7c1128b70a013e32e7ab3]['ip'] = $ip;
$this->functionData['membersModIPForm'][$count_d973849250c7c1128b70a013e32e7ab3]['inlineMsg'] = $inlineMsg;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- membersModIPFormMembers --*/
function membersModIPFormMembers($pages="",$members) {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['membersModIPFormMembers'] ) )
{
$count_a47f1157c9b50210836dee2f0799623c = is_array($this->functionData['membersModIPFormMembers']) ? count($this->functionData['membersModIPFormMembers']) : 0;
$this->functionData['membersModIPFormMembers'][$count_a47f1157c9b50210836dee2f0799623c]['pages'] = $pages;
$this->functionData['membersModIPFormMembers'][$count_a47f1157c9b50210836dee2f0799623c]['members'] = $members;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- membersModIPFormPosts --*/
function membersModIPFormPosts($count=0, $pageLinks='', $results) {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['membersModIPFormPosts'] ) )
{
$count_101c96ab63ec7621a0b4fe961faffc46 = is_array($this->functionData['membersModIPFormPosts']) ? count($this->functionData['membersModIPFormPosts']) : 0;
$this->functionData['membersModIPFormPosts'][$count_101c96ab63ec7621a0b4fe961faffc46]['count'] = $count;
$this->functionData['membersModIPFormPosts'][$count_101c96ab63ec7621a0b4fe961faffc46]['pageLinks'] = $pageLinks;
$this->functionData['membersModIPFormPosts'][$count_101c96ab63ec7621a0b4fe961faffc46]['results'] = $results;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- modAnnounceForm --*/
function modAnnounceForm($announce,$forum_html,$type,$editor_html,$msg="") {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['modAnnounceForm'] ) )
{
$count_1ac9fe395be9b76910dd2ae7d79836ff = is_array($this->functionData['modAnnounceForm']) ? count($this->functionData['modAnnounceForm']) : 0;
$this->functionData['modAnnounceForm'][$count_1ac9fe395be9b76910dd2ae7d79836ff]['announce'] = $announce;
$this->functionData['modAnnounceForm'][$count_1ac9fe395be9b76910dd2ae7d79836ff]['forum_html'] = $forum_html;
$this->functionData['modAnnounceForm'][$count_1ac9fe395be9b76910dd2ae7d79836ff]['type'] = $type;
$this->functionData['modAnnounceForm'][$count_1ac9fe395be9b76910dd2ae7d79836ff]['editor_html'] = $editor_html;
$this->functionData['modAnnounceForm'][$count_1ac9fe395be9b76910dd2ae7d79836ff]['msg'] = $msg;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- modAnnouncements --*/
function modAnnouncements($announcements) {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['modAnnouncements'] ) )
{
$count_37795ff847e332963b4aa5696c27135d = is_array($this->functionData['modAnnouncements']) ? count($this->functionData['modAnnouncements']) : 0;
$this->functionData['modAnnouncements'][$count_37795ff847e332963b4aa5696c27135d]['announcements'] = $announcements;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- modcpMessage --*/
function modcpMessage($message='') {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['modcpMessage'] ) )
{
$count_f6a52d41b720efdff101ebf61c87ba71 = is_array($this->functionData['modcpMessage']) ? count($this->functionData['modcpMessage']) : 0;
$this->functionData['modcpMessage'][$count_f6a52d41b720efdff101ebf61c87ba71]['message'] = $message;
}
$IPBHTML .= "<!--no data in this master skin-->";
return $IPBHTML;
}

/* -- modCPpost --*/
function modCPpost($post, $displayData, $topic, $type) {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['modCPpost'] ) )
{
$count_cf6d44d6359777d57645585e284a140c = is_array($this->functionData['modCPpost']) ? count($this->functionData['modCPpost']) : 0;
$this->functionData['modCPpost'][$count_cf6d44d6359777d57645585e284a140c]['post'] = $post;
$this->functionData['modCPpost'][$count_cf6d44d6359777d57645585e284a140c]['displayData'] = $displayData;
$this->functionData['modCPpost'][$count_cf6d44d6359777d57645585e284a140c]['topic'] = $topic;
$this->functionData['modCPpost'][$count_cf6d44d6359777d57645585e284a140c]['type'] = $type;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- modCPtopic --*/
function modCPtopic($topics, $pagelinks, $type, $sdelete=array()) {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['modCPtopic'] ) )
{
$count_f6ed9be433dd4c698d1e4cbd17a92477 = is_array($this->functionData['modCPtopic']) ? count($this->functionData['modCPtopic']) : 0;
$this->functionData['modCPtopic'][$count_f6ed9be433dd4c698d1e4cbd17a92477]['topics'] = $topics;
$this->functionData['modCPtopic'][$count_f6ed9be433dd4c698d1e4cbd17a92477]['pagelinks'] = $pagelinks;
$this->functionData['modCPtopic'][$count_f6ed9be433dd4c698d1e4cbd17a92477]['type'] = $type;
$this->functionData['modCPtopic'][$count_f6ed9be433dd4c698d1e4cbd17a92477]['sdelete'] = $sdelete;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- overview --*/
function overview() {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['overview'] ) )
{
$count_14cb6cfc22e408439fce3f7a25ebf042 = is_array($this->functionData['overview']) ? count($this->functionData['overview']) : 0;
}
$IPBHTML .= "<!--no data in this master skin-->";
return $IPBHTML;
}

/* -- portalPage --*/
function portalPage($output, $tabs=array(), $_activeNav=array()) {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['portalPage'] ) )
{
$count_e1cd68bb55135515d7ebe43e3dd42af9 = is_array($this->functionData['portalPage']) ? count($this->functionData['portalPage']) : 0;
$this->functionData['portalPage'][$count_e1cd68bb55135515d7ebe43e3dd42af9]['output'] = $output;
$this->functionData['portalPage'][$count_e1cd68bb55135515d7ebe43e3dd42af9]['tabs'] = $tabs;
$this->functionData['portalPage'][$count_e1cd68bb55135515d7ebe43e3dd42af9]['_activeNav'] = $_activeNav;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- subTabLoop --*/
function subTabLoop() {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['subTabLoop'] ) )
{
$count_6c25499a4706a2e95afde7506ce94d1b = is_array($this->functionData['subTabLoop']) ? count($this->functionData['subTabLoop']) : 0;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- unapprovedPosts --*/
function unapprovedPosts($posts, $pagelinks) {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['unapprovedPosts'] ) )
{
$count_314399efd79e38ade545ceec958093ee = is_array($this->functionData['unapprovedPosts']) ? count($this->functionData['unapprovedPosts']) : 0;
$this->functionData['unapprovedPosts'][$count_314399efd79e38ade545ceec958093ee]['posts'] = $posts;
$this->functionData['unapprovedPosts'][$count_314399efd79e38ade545ceec958093ee]['pagelinks'] = $pagelinks;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}

/* -- unapprovedTopics --*/
function unapprovedTopics($topics, $pagelinks) {
$IPBHTML = "";
if( IPSLib::locationHasHooks( 'skin_modcp', $this->_funcHooks['unapprovedTopics'] ) )
{
$count_898b01221da8ea5c17613d68e5d8a659 = is_array($this->functionData['unapprovedTopics']) ? count($this->functionData['unapprovedTopics']) : 0;
$this->functionData['unapprovedTopics'][$count_898b01221da8ea5c17613d68e5d8a659]['topics'] = $topics;
$this->functionData['unapprovedTopics'][$count_898b01221da8ea5c17613d68e5d8a659]['pagelinks'] = $pagelinks;
}
$IPBHTML .= "<!-- NoData -->";
return $IPBHTML;
}


}


/*--------------------------------------------------*/
/* END OF FILE                                      */
/*--------------------------------------------------*/

?>