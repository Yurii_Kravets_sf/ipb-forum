<?php

/*******************************************************
NOTE: This is a cache file generated by IP.Board on Thu, 06 Apr 2017 12:14:37 +0000 by master
Do not translate this file as you will lose your translations next time you edit via the ACP
Please translate via the ACP
*******************************************************/



$lang = array( 
'cancel_operation' => "Cancel Operation",
'convert_tool_run' => "Old settings were converted successfully.",
'convert_tool_title' => "Convert Old Settings",
'convert_tool_title_desc' => "With this tool you can convert old settings to the new system of forum icons.",
'current_image' => "Current Image",
'delete_icon' => "Delete Icon",
'editing_item' => "Editing Forum Icon",
'edit_item_button' => "Edit Forum",
'edit_settings' => "Edit Forum",
'error_no_forum_id' => "Can't find this forum. Please try again.",
'error_no_icon_field' => "No 'icon' field. This tool can't be used.",
'error_number__invalid_file_extension' => "Invalid file extension.",
'error_number__upload_failed_cant_move' => "Can't move file.",
'error_number__upload_failed_no_upload' => "Failed upload. Please try again.",
'error_number__upload_failed_possible_xss' => "Possible XSS attack!",
'error_number__upload_no_choosen_file' => "You didn't select file to upload.",
'error_number__upload_to_big' => "File what you trying upload is too big.",
'forumicons_overview' => "Forum Icons Overview",
'forums' => "Forums",
'icon' => "Icon",
'icon_desc' => "Select icon and upload it.",
'i_enabled' => "Enabled",
'i_enabled_desc' => "Do you want to show custom icon for this forum?",
'main_informations' => "Main Informations",
'noforums' => "Can't find any forum.",
'oror' => "<strong>...or...</strong>",
'rebuild_process_f' => "Processed forum %s ",
'redirectedit_item' => "Forum icon was edited successfully.",
're_percycle' => "Per Cycle",
're_rebuildcomp' => "<strong>Rebuild completed</strong><br /> ",
're_thisgoeshere' => "<strong>Up to %s processed so far, continuing...</strong>",
'run_tool' => "Run Tool",
'success_cache_updated' => "Icons cache was updated successfully.",
'success_icon_deleted' => "Icon was deleted successfully.",
'tools_list' => "Tools List",
'update_cache' => "Update Cache",
 ); 
