<?php
/**
 * SolutionrDEVs Application
 * (SD33) Pages
 *
 * @author      Dawid Baruch <dawid.baruch@solutiondevs.pl>
 * @copyright   (c) 2005 - 2012 SolutionDEVs
 * @package     SolutionDEVs Apps
 * @subpackage  PHP
 * @link        http://www.solutiondevs.pl
 * @link        http://www.ipsbeyond.pl
 * @version     1.0.0 
 *
 */

 
class cp_skin_sdpages extends output 
{
    public function overview( $stats ) 
    {
        $IPBHTML = "";
        //--starthtml--//
        
        $IPBHTML .= <<<HTML
<div class='section_title'>
	<h2>{$this->lang->words[ 'sd33_p_welcome_acp' ]} Страницы {$this->caches['app_cache']['sdpages']['app_version']}</h2>
</div>
<table width='100%'>
	<tr>
		<td width='40%' valign='top'>
			<div class='acp-box'>
		<h3>{$this->lang->words['sd33_p_statistics_header']}</h3>
		<table class='ipsTable'>
HTML;
        foreach( $stats as $key => $value )
        {
            $IPBHTML .= <<<HTML
            <tr>
				<td><strong>{$this->lang->words[ "sd33_p_{$key}" ]}:</strong></td>
				
				<td>
				    {$value}
				</td>
			</tr>
HTML;
        }
			
	    $IPBHTML .= <<<HTML
			
		</table>
	</div>
	
	</td>
	<td width='1%'>&nbsp;</td>
	<td width='59%' valign='top'>
	<div class='acp-box'>
		<h3>{$this->lang->words[ 'sd33_p_info' ]}</h3>
		<table class='ipsTable'>
			<tr>
					<td>
						<strong>{$this->lang->words[ 'sd33_p_author' ]}:</strong>
					</td>
					<td>
						Dawid Baruch
					</td>
				</tr>
				<tr>
					<td>
						<strong>{$this->lang->words[ 'sd33_p_author_website' ]}:</strong>
					</td>
					<td>
						<a href="http://www.solutiondevs.pl">SolutionDEVs.pl</a>
					</td>
				</tr>
				<tr>
					<td>
						<strong>{$this->lang->words[ 'sd33_p_support_website' ]}:</strong>
					</td>
					<td>
						<a href="http://www.ipsbeyond.pl">IPSBeyond.pl</a>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center;">
						<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="722MEWGXNEP4J">
<input type="image" src="https://www.paypal.com/en_US/i/btn/btn_donate_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypal.com/pl_PL/i/scr/pixel.gif" width="1" height="1">
</form>
					
					</td>
				</tr>
		</table>
	</div>
</td>
	</tr>
</table>
<br clear='both'>
HTML;


        //--endhtml--//
        return $IPBHTML;
    }

    public function footer() 
    {
        $IPBHTML = "";
        //--starthtml--//

        $year = date( "Y" );
        
        $IPBHTML .= <<<HTML
        <br class="clear" /><br />
        <div style="float:right; margin:10px;">
            (SD33) Страницы {$this->caches['app_cache']['sdpages']['app_version']} &copy; 2005 - {$year} &nbsp;<a style='text-decoration:none' href='http://www.ipbzona.ru' title='Стили, русские моды, хуки и компоненты'><span style="text-decoration:none;" class="ipsBadge badge_green">Перевод IpbZona.ru</span></a>
        </div>
HTML;

        //--endhtml--//
        return $IPBHTML;
    }
    
    public function managePages( $pages, $pagination )
    {
        $IPBHTML 		= "";

        //--starthtml--//
        $IPBHTML .= <<<HTML
<div class='section_title'>
	<h2>{$this->lang->words[ 'sd33_p_manage_pages_header' ]}</h2>
	<ul class='context_menu'>
		<li>
			<a href='{$this->settings['base_url']}{$this->form_code}&amp;do=add'><img src='{$this->settings['skin_acp_url']}/images/icons/add.png' alt='' />{$this->lang->words[ 'sd33_p_add_new_page_button' ]}</a>
		</li>
	</ul>
</div>

<div class='acp-box adv_controls' id='pages'>
 	<h3>{$this->lang->words[ 'sd33_p_manage_pages_header' ]}</h3>
	<div id='sortable_handle' class='ipsExpandable'>
HTML;

        if( count( $pages ) )
        {
            foreach( $pages as $page )
            {
                $IPBHTML .= <<<HTML
    	<div id='page_{$page[ 'page_id' ]}' class='isDraggable'>
    		<div class='category clearfix ipsControlRow'>
    		
    			<table class='ipsTable'>
    				<tr class='ipsControlRow isDraggable'>
    					<td class='col_drag'>
    						<span class='draghandle'>&nbsp;</span>
    					</td>
    					<td style='width: 20%'>
    						<strong class='title'>{$page[ 'page_name' ]}</strong><br />
    						<span class="desctext">{$page[ 'page_meta_title' ]}</span>
    					</td>
    					<td style='width: 10%;'>
                            <em>{$this->lang->words[ "sd33_p_page_active_{$page[ 'page_active' ]}" ]}</em>
    					</td>
    					<td style='width: 10%;'>
                            <em>{$this->lang->words[ "sd33_p_page_in_footer_{$page[ 'page_in_footer' ]}" ]}</em>
    					</td>
    					<td style='width: 10%;'>
                            <em>{$this->lang->words[ "sd33_p_page_in_header_{$page[ 'page_in_header' ]}" ]}</em>
    					</td>
    					<td style='width: 10%;'>
                            <em>{$this->lang->words[ "sd33_p_page_in_quick_nav_{$page[ 'page_in_quick_nav' ]}" ]}</em>
    					</td>
    					<td style='width: 20%;'>
                            {$this->registry->class_localization->getTime( $page[ 'page_add_date' ], '%d %B %Y - %H:%M:%S' )}<br />
                            {$this->registry->class_localization->getTime( $page[ 'page_edit_date' ], '%d %B %Y - %H:%M:%S' )}
    					</td>
    					<td class='col_buttons'>
    						<ul class='ipsControlStrip'>
            					<li class='i_edit'>
            						<a href='{$this->settings['base_url']}{$this->form_code}&amp;do=edit&amp;page_id={$page[ 'page_id']}' title='{$this->lang->words[ 'sd33_p_page_edit_list' ]}'>{$this->lang->words[ 'sd33_p_page_edit_list' ]}</a>
            					</li>
            					<li class='i_delete'>
            						<a href='{$this->settings['base_url']}{$this->form_code}&amp;do=delete&amp;page_id={$page[ 'page_id' ]}' onclick='return acp.confirmDelete("{$this->settings['base_url']}{$this->form_code}&amp;do=delete&amp;page_id={$page[ 'page_id' ]}");' title='{$this->lang->words[ 'sd33_p_page_delete_list' ]}'>{$this->lang->words[ 'sd33_p_page_delete_list' ]}</a>
            					</li>
            				</ul>
    					</td>
    				</tr>
    			</table>
    		</div>
    	</div>
HTML;
            }
        }
        else
        {
            $lang = sprintf($this->lang->words[ 'sd33_p_no_pages_records' ], "{$this->settings['base_url']}{$this->form_code}&amp;do=add" );
            
            $IPBHTML .= <<<HTML
            <table class='ipsTable'>
            	<tr>
            		<td class="no_messages">
            			{$lang}
            		</td>
            	</tr>
            </table>
HTML;
        }
        
        $IPBHTML .= <<<HTML
	</div>
</div>

<script type="text/javascript">
		dropItLikeItsHot = function( draggableObject, mouseObject )
		{
			var options = {
							method : 'post',
							parameters : Sortable.serialize( 'sortable_handle', { tag: 'div', name: 'pages' } )
						};
 
			new Ajax.Request( "{$this->settings['base_url']}{$this->form_code_js}&do=doreorder&md5check={$this->registry->adminFunctions->getSecurityKey()}".replace( /&amp;/g, '&' ), options );

			return false;
		};

		Sortable.create( 'sortable_handle', { tag: 'div', only: 'isDraggable', revert: true, format: 'page_([0-9]+)', onUpdate: dropItLikeItsHot, handle: 'draghandle' } );
	</script>

HTML;
        //--endhtml--//
        return $IPBHTML;
    }
    
    /**
     * Formularz dodawania/edycji pola
     * 
     * @param array $page
     */
    public function pageForm( array $page ) 
    {
        $IPBHTML = "";
        //--starthtml--//

        $IPBHTML .= <<<EOF
<div class='section_title'>
  <h2>{$page[ 'title' ]}</h2>
</div>


<form id='adminform' action='{$this->settings['base_url']}{$this->form_code}&amp;do={$page[ 'action' ]}' method='post'>
  <input type='hidden' name='_admin_auth_key' id='_admin_auth_key' value='{$this->registry->adminFunctions->getSecurityKey()}' />
  <input type='hidden' name='page_id' value='{$page[ 'page_id' ]}' />
<div class='acp-box'>
	<h3>{$page[ 'title' ]}</h3>
	
	<div id='tabstrip_pageForm' class='ipsTabBar with_left with_right'>
		<span class='tab_left'>&laquo;</span>
		<span class='tab_right'>&raquo;</span>
		<ul>
			<li id='tab_Basic'>{$this->lang->words[ 'sd33_p_basics' ]}</li>
			<li id='tab_Content'>{$this->lang->words[ 'sd33_p_content' ]}</li>
		</ul>
	</div>
	
	<div id='tabstrip_pageForm_content' class='ipsTabBar_content'>
	
		<!-- Basic -->
		<div id='tab_Basic_content'>
			<table class='ipsTable double_pad'>
				<tr>
					<td class='field_title'>
						<strong class='title'>{$this->lang->words[ 'sd33_p_form_page_name' ]}</strong>
					</td>
					<td class='field_field'>
						{$page[ 'page_name' ]}
					</td>
				</tr>
				<tr>
					<td class='field_title'>
						<strong class='title'>{$this->lang->words[ 'sd33_p_form_page_type' ]}</strong>
					</td>
					<td class='field_field'>
						{$page[ 'page_type' ]}
					</td>
				</tr>
				<tr>
					<td class='field_title'>
						<strong class='title'>{$this->lang->words[ 'sd33_p_form_page_active' ]}</strong>
					</td>
					<td class='field_field'>
						{$page[ 'page_active' ]}
					</td>
				</tr>
				<tr>
					<td class='field_title'>
						<strong class='title'>{$this->lang->words[ 'sd33_p_form_page_in_footer' ]}</strong>
					</td>
					<td class='field_field'>
						{$page[ 'page_in_footer' ]}
					</td>
				</tr>
				<tr>
					<td class='field_title'>
						<strong class='title'>{$this->lang->words[ 'sd33_p_form_page_in_header' ]}</strong>
					</td>
					<td class='field_field'>
						{$page[ 'page_in_header' ]}
					</td>
				</tr>
				<tr>
					<td class='field_title'>
						<strong class='title'>{$this->lang->words[ 'sd33_p_form_page_in_quick_nav' ]}</strong>
					</td>
					<td class='field_field'>
						{$page[ 'page_in_quick_nav' ]}
					</td>
				</tr>
				<tr>
					<td class='field_title'>
						<strong class='title'>{$this->lang->words[ 'sd33_p_form_page_ipb_wrapper' ]}</strong>
					</td>
					<td class='field_field'>
						{$page[ 'page_ipb_wrapper' ]}
					</td>
				</tr>
				<tr>
					<td class='field_title'>
						<strong class='title'>{$this->lang->words[ 'sd33_p_form_page_custom_permission' ]}</strong>
					</td>
					<td class='field_field'>
						{$page[ 'page_custom_permission' ]}
					</td>
				</tr>
				<tr>
					<td class='field_title'>
						<strong class='title'>{$this->lang->words[ 'sd33_p_form_page_permission' ]}</strong>
					</td>
					<td class='field_field'>
						{$page[ 'page_permission' ]}
						<br />
						<span class="desctext">{$this->lang->words[ 'sd33_p_form_page_permission_desc' ]}</span>
					</td>
				</tr>
	 		</table>
		</div>
		
		<!-- Content -->
		<div id='tab_Content_content'>
			<table class='ipsTable double_pad'>
				<tr>
					<td class='field_title'>
						<strong class='title'>{$this->lang->words[ 'sd33_p_form_page_meta_title' ]}</strong>
					</td>
					<td class='field_field'>
						{$page[ 'page_meta_title' ]}
						<br />
						<span class="desctext">{$this->lang->words[ 'sd33_p_form_page_meta_title_desc' ]}</span>
					</td>
				</tr>
				<tr>
					<td class='field_title'>
						<strong class='title'>{$this->lang->words[ 'sd33_p_form_page_content' ]}</strong>
					</td>
					<td class='field_field'>
						{$page[ 'page_content' ]}
						<br />
						<span class="desctext">{$this->lang->words[ 'sd33_p_form_page_content_desc' ]}</span>
					</td>
				</tr>
				<tr>
					<td class='field_title'>
						<strong class='title'>{$this->lang->words[ 'sd33_p_form_page_meta_keywords' ]}</strong>
					</td>
					<td class='field_field'>
						{$page[ 'page_meta_keywords' ]}
						<br />
						<span class="desctext">{$this->lang->words[ 'sd33_p_form_page_meta_keywords_desc' ]}</span>
					</td>
				</tr>
				<tr>
					<td class='field_title'>
						<strong class='title'>{$this->lang->words[ 'sd33_p_form_page_meta_description' ]}</strong>
					</td>
					<td class='field_field'>
						{$page[ 'page_meta_description' ]}
						<br />
						<span class="desctext">{$this->lang->words[ 'sd33_p_form_page_meta_description_desc' ]}</span>
					</td>
				</tr>
	 		</table>
		</div>
		
		<script type='text/javascript'>
			jQ("#tabstrip_pageForm").ipsTabBar({ tabWrap: "#tabstrip_pageForm_content", defaultTab: "tab_Basic" });
		</script>
	</div>
	<div class='acp-actionbar'>
		<input type='submit' value='{$page[ 'button' ]}' class='button primary' />
	</div>
</div>
</form>
EOF;

        //--endhtml--//
        return $IPBHTML;
    }
}