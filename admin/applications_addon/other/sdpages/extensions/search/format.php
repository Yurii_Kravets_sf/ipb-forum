<?php
/**
 * SolutionrDEVs Application
 * (SD33) Pages
 *
 * @author      Dawid Baruch <dawid.baruch@solutiondevs.pl>
 * @copyright   (c) 2005 - 2012 SolutionDEVs
 * @package     SolutionDEVs Apps
 * @subpackage  PHP
 * @link        http://www.solutiondevs.pl
 * @version     1.0.0 
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.<br />Author: Dawid Baruch <a href='http://www.solutiondevs.pl'><strong>SolutionDEVs.pl</strong></a>";
	exit();
}

class search_format_sdpages extends search_format
{
	/**
	 * Constructor
	 */
	public function __construct( ipsRegistry $registry )
	{
		parent::__construct( $registry );
		
		/* Language */
		$this->registry->class_localization->loadLanguageFile( array( 'public_lang' ), 'sdpages' );
	}
	
	/**
	 * Parse search results
	 *
	 * @access	private
	 * @param	array 	$r				Search result
	 * @return	array 	$html			Blocks of HTML
	 */
	public function parseAndFetchHtmlBlocks( $rows )
	{
		return parent::parseAndFetchHtmlBlocks( $rows );
	}
	
	/**
	 * Formats the forum search result for display
	 *
	 * @access	public izword
	 * @param	array   $search_row		Array of data from search_index
	 * @return	mixed	Formatted content, ready for display, or array containing a $sub section flag, and content
	 **/
	public function formatContent( $data )
	{
		return array( ipsRegistry::getClass( 'output' )->getTemplate( 'sdpages' )->pagesSearchResult( $data, IPSSearchRegistry::get('opt.searchType') == 'titles' ? true : false ), 0 );
	}

	/**
	 * Decides which type of search this was
	 *
	 * @access public
	 * @return array
	 */
	public function processResults( $ids )
	{
	    $rows = array();
		
		foreach( $ids as $i => $d )
		{
			$rows[ $i ] = $this->genericizeResults( $d );
		}
		
		return $rows;
	}
	
	/**
	 * Reassigns fields in a generic way for results output
	 *
	 * @param  array  $r
	 * @return array
	 **/
	public function genericizeResults( $r )
	{
		$r['app']                 = 'sdpages';
		$r['content']             = $r[ 'page_content' ];
		$r['content_title']       = $r[ 'page_name' ];
		$r['updated']             = $r[ 'page_edit_date' ];
		$r['type_2']              = 'pages';
		$r['type_id_2']           = $r[ 'page_id' ];
		$r['content_subtitle']    = $r[ 'page_name' ];

		return $r;
	}
}
?>