<?php
/**
 * SolutionrDEVs Application
 * (SD33) Pages
 *
 * @author      Dawid Baruch <dawid.baruch@solutiondevs.pl>
 * @copyright   (c) 2005 - 2012 SolutionDEVs
 * @package     SolutionDEVs Apps
 * @subpackage  PHP
 * @link        http://www.solutiondevs.pl
 * @version     1.0.0 
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.<br />Author: Dawid Baruch <a href='http://www.solutiondevs.pl'><strong>SolutionDEVs.pl</strong></a>";
	exit();
}

class search_engine_sdpages extends search_engine
{
	/**
	 * Constructor
	 */
	public function __construct( ipsRegistry $registry )
	{
		/* Hard limit */
		IPSSearchRegistry::set('set.hardLimit', ( ipsRegistry::$settings['search_hardlimit'] ) ? ipsRegistry::$settings['search_hardlimit'] : 200 );

		parent::__construct( $registry );
	}
	
	/**
	 * Decide what type of search we're using
	 *
	 * @access	public
	 * @return	array
	 */
	public function search()
	{
		/* INIT */ 
		$count       		= 0;
		$results     		= array();
		$sort_by     		= IPSSearchRegistry::get('in.search_sort_by');
		$sort_order         = IPSSearchRegistry::get('in.search_sort_order');
		$search_term        = IPSSearchRegistry::get('in.clean_search_term');
		$content_title_only = IPSSearchRegistry::get('opt.searchTitleOnly');
		$order_dir 			= ( $sort_order == 'asc' ) ? 'asc' : 'desc';
		$rows    			= array();
		$count   			= 0;
		$c                  = 0;
		$got     			= 0;
		$sortKey			= '';
		$sortType			= '';
		$rows               = array();

		/* Sorting */
	    switch( $sort_by )
		{
			default:
			case 'date':
				$sortKey  = 'page_edit_date';
				$sortType = 'numerical';
			break;
			
			case 'title':
				$sortKey  = 'page_name';
				$sortType = 'string';
			break;
		}
		
		$where = $this->_buildWhereStatement( $search_term, $content_title_only );
		
		$count	= $this->DB->buildAndFetch(
											array( 
													'select'	=> 'COUNT(*) as total_results',
													'from'		=> 'sd_pages',
 													'where'		=> $where,													
										)  );

		if( $count[ 'total_results' ] )
		{
    		/* Fetch data */
    		$this->DB->build( array( 
    									'select'   => "*",
    									'from'	   => 'sd_pages',
    									'where'	   => $where,
    									'limit'    => array(0, IPSSearchRegistry::get('set.hardLimit') + 1),
    									'order'	   => "{$sortKey} {$order_dir}",
    						)	);
    						
    		$this->DB->execute();
    		
    		IPSText::getTextClass( 'bbcode' )->parse_smilies			= 1;
    		IPSText::getTextClass( 'bbcode' )->parse_html				= 1;
    		IPSText::getTextClass( 'bbcode' )->parse_nl2br				= 1;
    		IPSText::getTextClass( 'bbcode' )->parse_bbcode				= 1;
    		IPSText::getTextClass( 'bbcode' )->parsing_section			= 'page_content';
    
    		
    		
    		/* Fetch to sort */
    		while ( $r = $this->DB->fetch() )
    		{
    		    $r[ 'page_content' ]	 = IPSText::getTextClass( 'bbcode' )->preDisplayParse( $r[ 'page_content' ] );
                $r[ 'page_content' ]     = IPSText::truncate( IPSText::getTextClass( 'bbcode' )->stripAllTags( strip_tags( $r[ 'page_content' ], '<br>' ) ), 500 );
                
    			$rows[] = $r;
    		}
		}
		
		/* Return it */
		return array( 'count' => $count[ 'total_results' ], 'resultSet' => $rows );
	}
	
	/**
	 * Builds the where portion of a search string
	 *
	 * @access	private
	 * @param	string	$search_term		The string to use in the search
	 * @param	bool	$content_title_only	Search only title records
	 * @return	string
	 */
	private function _buildWhereStatement( $search_term, $content_title_only=false )
	{
		/* INI */
		$where_clause	= array();
		$searchInCats	= array();
		
		if( $search_term )
		{
			$search_term	= trim($search_term);
			
			$where_clause[] = "(page_name LIKE '%{$search_term}%' OR page_content LIKE '%{$search_term}%')";
		}
		
		/* Date Restrict */
		if( $this->search_begin_timestamp && $this->search_end_timestamp )
		{
			$where_clause[] = $this->DB->buildBetween( "page_edit_date", $this->search_begin_timestamp, $this->search_end_timestamp );
		}
		else
		{
			if( $this->search_begin_timestamp )
			{
				$where_clause[] = "page_edit_date > {$this->search_begin_timestamp}";
			}
			
			if( $this->search_end_timestamp )
			{
				$where_clause[] = "page_edit_date < {$this->search_end_timestamp}";
			}
		}
		
		/* Add in AND where conditions */
		if( isset( $this->whereConditions['AND'] ) && count( $this->whereConditions['AND'] ) )
		{
			$where_clause = array_merge( $where_clause, $this->whereConditions['AND'] );
		}
		
		/* ADD in OR where conditions */
		if( isset( $this->whereConditions['OR'] ) && count( $this->whereConditions['OR'] ) )
		{
			$where_clause[] = '( ' . implode( ' OR ', $this->whereConditions['OR'] ) . ' )';
		}
		
		$where_clause[] = "page_active=1 AND (page_permission='*' OR (page_custom_permission=1 AND page_permission LIKE '%,{$this->memberData[ 'member_group_id' ]},%'))";
		
		/* Build and return the string */
		return implode( " AND ", $where_clause );
	}
	
	/**
	 * Remap standard columns (Apps can override )
	 *
	 * @access	public
	 * @param	string	$column		sql table column for this condition
	 * @return	string				column
	 * @return	void
	 */
	public function remapColumn( $column )
	{
		return $column;
	}
		
	/**
	 * Returns an array used in the searchplugin's setCondition method
	 *
	 * @access	public
	 * @param	array 	$data	Array of forums to view
	 * @return	array 	Array with column, operator, and value keys, for use in the setCondition call
	 **/
	public function buildFilterSQL( $data )
	{
		return array();
	}

	/**
	 * Can handle boolean searching
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function isBoolean()
	{
		return true;
	}
}
?>