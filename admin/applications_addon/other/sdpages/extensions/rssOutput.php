<?php
/**
 * SolutionrDEVs Application
 * (SD33) Pages
 *
 * @author      Dawid Baruch <dawid.baruch@solutiondevs.pl>
 * @copyright   (c) 2005 - 2012 SolutionDEVs
 * @package     SolutionDEVs Apps
 * @subpackage  PHP
 * @link        http://www.solutiondevs.pl
 * @link        http://www.ipsbeyond.pl
 * @version     1.0.0 
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.<br />Author: Dawid Baruch <a href='http://www.solutiondevs.pl'><strong>SolutionDEVs.pl</strong></a>";
	exit();
}

class rss_output_sdpages
{
	/**
	 * Expiration date
	 *
	 * @access	protected
	 * @var		integer			Expiration timestamp
	 */
	protected $expires			= 0;
	
	/**
	 * RSS object
	 *
	 * @access	public
	 * @var		object
	 */
	public $class_rss;
	
	/**
     * Main application classs
     *
     * @var sdpages
     */
    private $sdpages;

	/**
	 * Grab the RSS links
	 *
	 * @access	public
	 * @return	string		RSS document
	 */
	public function __construct()
	{
		$this->registry = ipsRegistry::instance();
        $this->settings   =& $this->registry->fetchSettings();
                

		// Load library
		require_once( IPSLib::getAppDir( 'sdpages' ) . '/sources/classSdPages.php' );
        $this->sdpages = new sdpages( $this->registry );
                
        ipsRegistry::getClass( 'class_localization' )->loadLanguageFile( array( 'public_lang' ), 'sdpages' );
	}
		
	public function getRssLinks()
	{		
		$return	= array();

		if( ipsRegistry::$settings[ 'sd33_p_rss_system' ] )
		{
	        $return[] = array( 'title' => ipsRegistry::getClass('class_localization')->words[ 'sd33_p_rss_title' ], 'url' => ipsRegistry::getClass('output')->formatUrl( ipsRegistry::$settings['board_url'] . "/index.php?app=core&amp;module=global&amp;section=rss&amp;type=sdpages", true, 'section=rss' ) );
	    }

	    return $return;
	}
	
	/**
	 * Grab the RSS document content and return it
	 *
	 * @access	public
	 * @return	string		RSS document
	 */
	public function returnRSSDocument()
	{
		//--------------------------------------------
		// Require classes
		//--------------------------------------------
		
		$classToLoad				= IPSLib::loadLibrary( IPS_KERNEL_PATH . 'classRss.php', 'classRss' );
		$this->class_rss			= new $classToLoad();
		$this->class_rss->doc_type	= ipsRegistry::$settings[ 'gb_char_set' ];
		
		//-----------------------------------------
		// Enabled?
		//-----------------------------------------
		
		if( !ipsRegistry::$settings[ 'sd33_p_rss_system' ] )
		{
			return $this->_returnError( ipsRegistry::getClass('class_localization')->words[ 'sd33_p_rss_disabled' ] );
		}
		
		//-----------------------------------------
        // Load and config the post parser
        //-----------------------------------------
        
        IPSText::getTextClass( 'bbcode' )->bypass_badwords	= 0;
        
		$channel_id = $this->class_rss->createNewChannel( array( 'title'		=> ipsRegistry::getClass('class_localization')->words[ 'sd33_p_rss_title' ],
																 'link'			=> ipsRegistry::getClass('output')->formatUrl( ipsRegistry::$settings[ 'board_url' ] . '/index.php?app=sdpages', 'false', 'app=sdpages' ),
																 'pubDate'		=> $this->class_rss->formatDate( time() ),
																 'ttl'			=> 30 * 60,
																 'description'	=> ipsRegistry::getClass('class_localization')->words[ 'sd33_p_rss_desc' ],
		                                                         'generator'	=> '(SD33) Pages'
													)      );

		$pages = $this->sdpages->getPagesForRss();
		
	    if ( $pages ) 
	    {
            foreach( $pages as $page ) 
            {
            	$this->class_rss->addItemToChannel( $channel_id, array( 'title'			=> $page[ 'page_name' ],
    																	'link'			=> ipsRegistry::getClass('output')->formatUrl( ipsRegistry::$settings['board_url'] . "/index.php?app=sdpages&amp;module=pages&amp;section=page&amp;page_id={$page[ 'page_id' ]}", $page[ 'page_name_seo' ], 'site' ),
    																	'description'	=> $page[ 'page_content' ],
    																	'pubDate'		=> $this->class_rss->formatDate( $page[ 'page_edit_date' ] ),
    																	'guid'			=> $page[ 'page_id' ]
    									  )                    );

            }
        }
		
		
		$this->class_rss->createRssDocument();
		
		$this->class_rss->rss_document = ipsRegistry::getClass('output')->replaceMacros( $this->class_rss->rss_document );

		return $this->class_rss->rss_document;
	}
	
	/**
	 * Grab the RSS document expiration timestamp
	 *
	 * @access	public
	 * @return	integer		Expiration timestamp
	 */
	public function grabExpiryDate()
	{
		// Generated on the fly, so just return expiry of one hour
		return time() + 3600;
	}
	
	/**
	 * Return an error document
	 *
	 * @access	protected
	 * @param	string			Error message
	 * @return	string			XML error document for RSS request
	 */
	protected function _returnError( $error='' )
	{
	    ipsRegistry::getClass( 'class_localization' )->loadLanguageFile( array( 'public_lang' ), 'sdpages' );
	    
		$channel_id = $this->class_rss->createNewChannel( array( 
															'title'			=> ipsRegistry::getClass('class_localization')->words[ 'sd33_p_rss_disabled' ],
															'link'			=> ipsRegistry::getClass('output')->formatUrl( ipsRegistry::$settings['board_url'] . "/index.php?app=sdpages", 'false', 'app=sdpages' ),
				 											'description'	=> ipsRegistry::getClass('class_localization')->words[ 'sd33_p_rss_desc' ],
				 											'pubDate'		=> $this->class_rss->formatDate( time() ),
				 											'webMaster'		=> ipsRegistry::$settings['email_in'] . " (" . ipsRegistry::$settings['board_name'] . ")",
				 											'generator'		=> '(SD33) Pages'
				 										)		);

		$this->class_rss->addItemToChannel( $channel_id, array( 
														'title'			=> ipsRegistry::getClass('class_localization')->words[ 'sd33_p_rss_error_message' ],
			 										    'link'			=> ipsRegistry::getClass('output')->formatUrl( ipsRegistry::$settings['board_url'] . "/index.php?app=sdpages", 'false', 'app=sdpages' ),
			 										    'description'	=> $error,
			 										    'pubDate'		=> $this->class_rss->formatDate( time() ),
			 										    'guid'			=> ipsRegistry::getClass('output')->formatUrl( ipsRegistry::$settings['board_url'] . "/index.php?app=sdpages&error=1", 'false', 'app=sdpages' ) ) );

		//-----------------------------------------
		// Do the output
		//-----------------------------------------

		$this->class_rss->createRssDocument();

		return $this->class_rss->rss_document;
	}
}