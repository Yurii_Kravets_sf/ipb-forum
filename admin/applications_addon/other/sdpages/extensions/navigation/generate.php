<?php
/**
 * SolutionrDEVs Application
 * (SD33) Pages
 *
 * @author      Dawid Baruch <dawid.baruch@solutiondevs.pl>
 * @copyright   (c) 2005 - 2012 SolutionDEVs
 * @package     SolutionDEVs Apps
 * @subpackage  PHP
 * @link        http://www.solutiondevs.pl
 * @link        http://www.ipsbeyond.pl
 * @version     1.0.0 
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.<br />Author: Dawid Baruch <a href='http://www.solutiondevs.pl'><strong>SolutionDEVs.pl</strong></a>";
	exit();
}

class navigation_sdpages
{
	/**
	 * Registry Object Shortcuts
	 *
	 * @var		$registry
	 * @var		$DB
	 * @var		$settings
	 * @var		$request
	 * @var		$lang
	 * @var		$member
	 * @var		$memberData
	 * @var		$cache
	 * @var		$caches
	 */
	protected $registry;
	protected $DB;
	protected $settings;
	protected $request;
	protected $lang;
	protected $member;
	protected $memberData;
	protected $cache;
	protected $caches;

	/**
     * Main application classs
     *
     * @var sdpages
     */
    private $sdpages;

	/**
	 * Constructor
	 *
	 * @return	@e void
	 */
	public function __construct() 
	{
		$this->registry		=  ipsRegistry::instance();
		$this->DB			=  $this->registry->DB();
		$this->settings		=& $this->registry->fetchSettings();
		$this->request		=& $this->registry->fetchRequest();
		$this->member		=  $this->registry->member();
		$this->memberData	=& $this->registry->member()->fetchMemberData();
		$this->cache		=  $this->registry->cache();
		$this->caches		=& $this->registry->cache()->fetchCaches();
		$this->lang			=  $this->registry->class_localization;
		
		require_once( IPSLib::getAppDir( 'sdpages' ) . '/sources/classSdPages.php' );
        $this->sdpages = new sdpages( $this->registry );
	}
	
	/**
	 * Return the tab title
	 *
	 * @return	@e string
	 */
	public function getTabName()
	{ 
		return IPSLib::getAppTitle( 'sdpages' );
	}
	
	/**
	 * Returns navigation data
	 *
	 * @return	@e array	array( array( 0 => array( 'title' => 'x', 'url' => 'x' ) ) );
	 */
	public function getNavigationData()
	{
		$blocks	= array();
		$links	= array();
	
		$pages = $this->sdpages->getPagesInQuickNav();
		
		
	    foreach( $pages as $page )
		{
			$links[] = array( 'important' => true, 'depth' => 0, 'title' => $page[ 'page_name' ], 'url' => $this->registry->getClass('output')->buildSEOUrl( "app=sdpages&amp;module=pages&amp;section=page&amp;page_id={$page[ 'page_id' ]}", 'public', $page[ 'page_name_seo' ], 'site' ) );
		}
		
		$blocks[] = array( 'title' => IPSLib::getAppTitle( 'sdpages' ), 'links' => $links );
		
		return $blocks;
	}
}