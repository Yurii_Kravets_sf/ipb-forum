<?php
/**
 * SolutionrDEVs Application
 * (SD33) Pages
 *
 * @author      Dawid Baruch <dawid.baruch@solutiondevs.pl>
 * @copyright   (c) 2005 - 2012 SolutionDEVs
 * @package     SolutionDEVs Apps
 * @subpackage  PHP
 * @link        http://www.solutiondevs.pl
 * @link        http://www.ipsbeyond.pl
 * @version     1.0.0 
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.<br />Author: Dawid Baruch <a href='http://www.solutiondevs.pl'><strong>SolutionDEVs.pl</strong></a>";
	exit();
}

class publicSessions__sdpages
{
	/**
	 * Return session variables for this application
	 *
	 * current_appcomponent, current_module and current_section are automatically
	 * stored. This function allows you to add specific variables in.
	 *
	 * @access	public
	 * @author	Matt Mecham
	 * @return	array
	 */
	public function getSessionVariables()
	{
		/* Init */
        $array = array( 'location_1_type'   => 'sdpages',
                        'location_1_id'     => 0,
                        'location_2_type'   => '',
                        'location_2_id'     => 0,
                        'location_3_type'   => '',
                        'location_3_id'     => 0  );
        
        return $array;
	}

	/**
	 * Parse/format the online list data for the records
	 *
	 * @access	public
	 * @author	Brandon Farber
	 * @param	array 			Online list rows to check against
	 * @return	array 			Online list rows parsed
	 */
	public function parseOnlineEntries( $rows )
	{
		/* Anything? */
        if ( ! is_array( $rows ) || ! count( $rows ) )
        {
            return $rows;
        }
        
        /* Init */
        $registry       =    ipsRegistry::instance();
        
        /* Grab us some lang */
        $registry->getClass( 'class_localization' )->loadLanguageFile( array( 'public_lang' ), 'sdpages' );
        
        /* Iterate thru... */
        $final = array();
        foreach( $rows as $row )
        {
            if ( $row[ 'current_appcomponent' ] != 'sdpages' )
            {
                $final[ $row[ 'id' ] ] = $row;              
                continue;
            }
            
            /* Set basic... */
            $row[ 'where_line' ]      = $registry->getClass( 'class_localization' )->words[ 'WHERE_sdpages' ];
            $row[ 'where_line_more' ] = 'Index';
            $row[ 'where_link' ]      = 'app=sdpages';
            $row[ '_whereLinkSeo' ]   = ipsRegistry::getClass( 'output' )->formatUrl( ipsRegistry::getClass( 'output' )->buildUrl( 'app=sdpages', 'public' ), 'false', 'app=sdpages' );
            
            /* Save */
            $final[ $row[ 'id' ] ] = $row;
        }
                
        return $final;
	}
}