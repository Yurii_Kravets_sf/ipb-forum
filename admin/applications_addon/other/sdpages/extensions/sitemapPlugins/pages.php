<?php
/**
 * SolutionrDEVs Application
 * (SD33) Pages
 *
 * @author      Dawid Baruch <dawid.baruch@solutiondevs.pl>
 * @copyright   (c) 2005 - 2012 SolutionDEVs
 * @package     SolutionDEVs Apps
 * @subpackage  PHP
 * @link        http://www.solutiondevs.pl
 * @link        http://www.ipsbeyond.pl
 * @version     1.0.0 
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.<br />Author: Dawid Baruch <a href='http://www.solutiondevs.pl'><strong>SolutionDEVs.pl</strong></a>";
	exit();
}

require_once( IPSLib::getAppDir( 'ipseo' ) . '/sources/sitemapplugin.php');/*noLibHook*/

class sitemap_ipseo_pages extends ipseoSitemapPlugin
{
	public function generate()
	{
		while($curTopics < $maxTopics)
		{
			if(ipSeo_SitemapGenerator::isCronJob())
			{
				print $curTopics . ', ';
				sleep(0.5);
			}
			
			
			$this->DB->build( array( 
                                'select' => 'page_id, page_name, page_name_seo, page_content, page_edit_date',
                                'from'   => 'sd_pages',
	                            'order'	 => 'page_edit_date DESC',
	                            'where'	 => "page_active=1 AND (page_permission='*' OR (page_custom_permission=1 AND page_permission LIKE '%,{$this->settings[ 'guest_group' ]},%'))",
                        )   );
                        
			$result = $this->DB->execute();
			
			if($result)
			{				
				while($row = $this->DB->fetch($result))
				{
				    $url = $this->settings['board_url'] . '/index.php?app=sdpages&amp;module=pages&amp;section=page&amp;page_id=' . $row[ 'page_id' ];
					$url = ipSeo_FURL::build($url, 'none', $row[ 'page_name_seo' ], 'site');
            
            		$this->sitemap->addURL( $url, $row[ 'page_edit_date' ], $this->calculatePriority( $row ) );
				}
			}
						
		}
		
	    if(ipSeo_SitemapGenerator::isCronJob())
		{
			print 'Done' . PHP_EOL;
		}
	}
	
	protected function calculatePriority( $page, $subPage = false)
	{
		$priority = 0.6;
		
		// Boost topics where start date is today:
		if($page[ 'page_edit_date' ] > (time() - 86400))
		{
			$priority = $priority + 0.1;
		}
		
		if($priority > 1)
		{
			$priority = 1.0;
		}
		elseif($priority < 0)
		{
			$priority = 0.0;
		}
		
		return $priority;
	}
}