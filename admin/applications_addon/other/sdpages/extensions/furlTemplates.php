<?php
/**
 * SolutionrDEVs Application
 * (SD33) Pages
 *
 * @author      Dawid Baruch <dawid.baruch@solutiondevs.pl>
 * @copyright   (c) 2005 - 2012 SolutionDEVs
 * @package     SolutionDEVs Apps
 * @subpackage  PHP
 * @link        http://www.solutiondevs.pl
 * @link        http://www.ipsbeyond.pl
 * @version     1.0.0 
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.<br />Author: Dawid Baruch <a href='http://www.solutiondevs.pl'><strong>SolutionDEVs.pl</strong></a>";
	exit();
}

$_SEOTEMPLATES = array(
						
						
						'app=sdpages'       => array( 
                                                        'app'		      => 'sdpages',
                                                        'allowRedirect' => 1,
                                                        'out'           => array( '#app=sdpages$#i', 'sites/'  ),
                                                        'in'            => array( 
																					'regex'   => '#/sites($|\/)#i',
                                                        				        	'matches' => array( 
                                                                                                        array( 'app', 'sdpages' ) 
                                                                                                    ) 
                                                                                    ) 
                                                    ),
						'site' => array( 
											'app'			=> 'sdpages',
											'allowRedirect' => 1,
											'out'			=> array( '#app=sdpages(?:&|&amp;)module=pages(?:&|&amp;)section=page(?:&|&amp;)page_id=(.+?)(&|$)#i', 'site/$1-#{__title__}/$2' ),
											'in'			=> array( 
																		'regex'		=> '#/site/(\d+?)-#i',
																		'matches'	=> array( 
																								array( 'app'		, 'sdpages' ),
																								array( 'module'		, 'pages' ),
																								array( 'section'	, 'page' ),
																								array( 'page_id'	, '$1' )
																							)
																	) 
										),
);
?>