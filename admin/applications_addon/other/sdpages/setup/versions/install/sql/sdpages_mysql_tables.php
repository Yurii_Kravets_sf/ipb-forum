<?php
/**
 * SolutionrDEVs Application
 * (SD33) Pages
 *
 * @author      Dawid Baruch <dawid.baruch@solutiondevs.pl>
 * @copyright   (c) 2005 - 2012 SolutionDEVs
 * @package     SolutionDEVs Apps
 * @subpackage  PHP
 * @link        http://www.solutiondevs.pl
 * @link        http://www.ipsbeyond.pl
 * @version     1.0.0 
 *
 */

/**
 * Tabela podsumowania
 */
$TABLE[] = "CREATE TABLE sd_pages (
  page_id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  page_name VARCHAR(255) NOT NULL ,
  page_name_seo VARCHAR(255) NOT NULL ,
  page_meta_title VARCHAR(255) NOT NULL ,
  page_meta_keywords TEXT NOT NULL ,
  page_meta_description TEXT NOT NULL ,
  page_content MEDIUMTEXT NOT NULL ,
  page_active TINYINT(3) UNSIGNED NOT NULL DEFAULT 1 ,
  page_in_footer TINYINT(3) UNSIGNED NOT NULL DEFAULT 0 ,
  page_in_header TINYINT(3) UNSIGNED NOT NULL DEFAULT 0 ,
  page_add_date INT(10) UNSIGNED NOT NULL ,
  page_edit_date INT(10) UNSIGNED NOT NULL DEFAULT 0 ,
  page_ipb_wrapper TINYINT(3) UNSIGNED NOT NULL DEFAULT 1 ,
  page_in_quick_nav TINYINT(3) UNSIGNED NOT NULL DEFAULT 0 ,
  page_type VARCHAR(255) NOT NULL ,
  page_custom_permission TINYINT(3) UNSIGNED NOT NULL DEFAULT 0 ,
  page_permission TEXT NOT NULL ,
  page_position INT(10) UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (page_id) );";

?>