<?php
/**
 * SolutionrDEVs Application
 * (SD33) Pages
 *
 * @author      Dawid Baruch <dawid.baruch@solutiondevs.pl>
 * @copyright   (c) 2005 - 2012 SolutionDEVs
 * @package     SolutionDEVs Apps
 * @subpackage  PHP
 * @link        http://www.solutiondevs.pl
 * @link        http://www.ipsbeyond.pl
 * @version     1.0.0 
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.<br />Author: Dawid Baruch <a href='http://www.solutiondevs.pl'><strong>SolutionDEVs.pl</strong></a>";
	exit();
}

class app_class_sdpages
{
	/**
	 * Constructor
	 *
	 * @access	public
	 * @param	object	ipsRegistry
	 * @return	void
	 */
	public function __construct( ipsRegistry $registry )
	{
	    if ( IN_ACP )
		{
			$registry->getClass('class_localization')->loadLanguageFile( array( 'admin_lang' ), 'sdpages' );
		}
		else
		{
    		$registry->getClass('class_localization')->loadLanguageFile( array( 'public_lang' ), 'sdpages' );
		}
	}
}