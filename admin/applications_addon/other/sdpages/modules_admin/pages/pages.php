<?php
/**
 * SolutionrDEVs Application
 * (SD33) Pages
 *
 * @author      Dawid Baruch <dawid.baruch@solutiondevs.pl>
 * @copyright   (c) 2005 - 2012 SolutionDEVs
 * @package     SolutionDEVs Apps
 * @subpackage  PHP
 * @link        http://www.solutiondevs.pl
 * @link        http://www.ipsbeyond.pl
 * @version     1.0.0 
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.<br />Author: Dawid Baruch <a href='http://www.solutiondevs.pl'><strong>SolutionDEVs.pl</strong></a>";
	exit();
}

class admin_sdpages_pages_pages extends ipsCommand 
{
    /**
     * Skin object
     *
     * @access	private
     * @var	object			Skin templates
     */
    private $html;

    /**
     * Shortcut for url
     *
     * @access	private
     * @var	string			URL shortcut
     */
    private $form_code;

    /**
     * Shortcut for url (javascript)
     *
     * @access	private
     * @var	string			JS URL shortcut
     */
    private $form_code_js;

    /**
     * class Public Permissions
     *
     * @var classPublicPermissions
     */
    private $permissions;

    /**
     * Main application classs
     *
     * @var sdpages
     */
    private $sdpages;
    
    
    /**
     * Main class entry point
     *
     * @access	public
     * @param	object		ipsRegistry reference
     * @return	void		[Outputs to screen]
     */
    public function doExecute( ipsRegistry $registry ) 
    {
        $this->html = $this->registry->output->loadTemplate( 'cp_skin_sdpages' );

        $this->html->form_code    = $this->form_code    = 'module=pages&amp;section=pages';
        $this->html->form_code_js = $this->form_code_js = 'module=pages&section=pages';

        ipsRegistry::getClass('class_localization')->loadLanguageFile( array( 'admin_lang' ), 'sdpages' );
        ipsRegistry::getClass('class_localization')->loadLanguageFile( array( 'admin_tools' ), 'core' );
        
        require_once( IPS_ROOT_PATH . 'sources/classes/class_public_permissions.php' );
        $this->permissions = new classPublicPermissions( ipsRegistry::instance() );

        require_once( IPSLib::getAppDir( 'sdpages' ) . '/sources/classSdPages.php' );
        $this->sdpages = new sdpages( $this->registry );

        switch( $this->request[ 'do' ] ) 
        {
            case 'edit':
                $this->registry->getClass('class_permissions')->checkPermissionAutoMsg( 'sdpages_can_edit' );
                $this->showForm( 'edit' );
            break;
            
            case 'add':
                $this->registry->getClass('class_permissions')->checkPermissionAutoMsg( 'sdpages_can_add' );
                $this->showForm();
            break;
            
            case 'doedit':
                $this->registry->getClass('class_permissions')->checkPermissionAutoMsg( 'sdpages_can_edit' );
                $this->saveForm( 'edit' );
            break;
            
            case 'doadd':
                $this->registry->getClass('class_permissions')->checkPermissionAutoMsg( 'sdpages_can_add' );
                $this->saveForm();
            break;
            
            case 'delete':
                $this->registry->getClass('class_permissions')->checkPermissionAutoMsg( 'sdpages_can_delete' );
                $this->delPage();
            break;
            
            case 'doreorder':
                $this->registry->getClass('class_permissions')->checkPermissionAutoMsg( 'sdpages_can_reorder' );
                $this->reorderPage();
            break;
            
            case 'list':
            default:
                $this->registry->getClass('class_permissions')->checkPermissionAutoMsg( 'sdpages_can_list' );
                $this->pagesList();
            break;
        }

        //-----------------------------------------
        // Output
        //-----------------------------------------
        
        $this->registry->output->html .= $this->html->footer();
        $this->registry->output->html_main .= $this->registry->output->global_template->global_frame_wrapper();
        $this->registry->output->sendOutput();
    }

    /**
     * Pages list
     */
    private function pagesList() 
    {
        $amountOfEntries    = $this->sdpages->getAmountOfPages();
                                            
        $st                 = intval( $this->request[ 'st' ] ) > 0 ? intval( $this->request[ 'st' ] ) : 0;
        
        $pagination         = $this->registry->output->generatePagination( array( 
                                                                            'totalItems'        => $amountOfEntries,
                                                                            'itemsPerPage'      => sdpages::$itemsPerPage,
                                                                            'currentStartValue' => $st,
                                                                            'baseUrl'           => $this->settings['base_url'] . $this->form_code
                                                                    ) );
                                                                    
        $pages = $this->sdpages->getPagesList( $st );
        
        $this->registry->output->html .= $this->html->managePages( $pages, $pagination );
    }
    
    /**
     * Add/Edit page
     * 
     * @param string $type Type
     */
    private function showForm( $type = 'add' )
    {
        if( $type == 'edit' ) 
        {
            $id = intval( $this->request[ 'page_id' ] );

            if( !$id )
            {
                $this->registry->output->showError( $this->lang->words[ 'sd33_p_page_bad_id' ], 'SD33P0001' );
            }
            
            $page = $this->sdpages->getPage( $id );
            
            if( !isset( $page[ 'page_id' ] ) ||  $page[ 'page_id' ] != $id )
            {
                $this->registry->output->showError( $this->lang->words[ 'sd33_p_page_bad_id' ], 'SD33P0002' );
            }
            
            $page[ 'title' ]     = sprintf( $this->lang->words[ 'sd33_p_edit_page_title' ], $page[ 'page_name' ] );
            $page[ 'action' ]    = 'doedit';
            $page[ 'button' ]    = $this->lang->words[ 'sd33_p_edit_page_button' ];
        }
        else 
        {
            $page = array(
                'page_name'                 => '',
                'page_meta_title'           => '',
                'page_meta_keywords'        => '',
                'page_meta_description'	    => '',
                'page_content'              => '',
                'page_active'               => 1,
                'page_in_footer'            => 0,
                'page_in_header'	        => 0,
                'page_ipb_wrapper'	        => 1,
                'page_in_quick_nav'	        => 1,
                'page_type'	                => '',
                'page_custom_permission'    => 0,
                'page_permission'	        => '',
                'page_id'	                => 0,
                'title' 	                => $this->lang->words[ 'sd33_p_add_page_title' ],
                'action'                    => 'doadd',
                'button' 		            => $this->lang->words[ 'sd33_p_add_page_button' ],
            );
        }
        
		/* Show description in editor, get editor */
        $classToLoad = IPSLib::loadLibrary( IPS_ROOT_PATH . 'sources/classes/editor/composite.php', 'classes_editor_composite' );
        $_editor = new $classToLoad();
        
        $_editor->setAllowBbcode( true );
        $_editor->setAllowSmilies( false );
        $_editor->setAllowHtml( true );
        
        $pageType = array(
            array( 'bbcode', $this->lang->words[ 'sd33_p_page_type_standard' ] ),
            array( 'html', $this->lang->words[ 'sd33_p_page_type_html' ] ),
        );
        
        $dropdown = array();
        
        $this->DB->build( array( 'select' => '*', 'from' => 'groups', 'order' => 'g_title ASC' ) );
    	$this->DB->execute();
    	
    	while( $row = $this->DB->fetch() )
    	{
    		if ( $row['g_access_cp' ] )
    		{
    			$row[ 'g_title' ] .= ' ' . $this->lang->words[ 'setting_staff_tag' ] . ' ';
    		}
    		
    		$dropdown[] = array( $row[ 'g_id' ], $row[ 'g_title' ] );
    	}
        
        
        /* Set content in editor */
        $page[ 'page_content' ]             = $_editor->show( 'page_content', array( 'isHtml' => $page[ 'page_type' ] == 'html' ), $page[ 'page_content' ] );
        $page[ 'page_type' ]                = $this->registry->output->formDropdown( 'page_type', $pageType, $page[ 'page_type' ] );
        $page[ 'page_name' ]                = $this->registry->output->formInput( 'page_name', $page[ 'page_name' ] );
        $page[ 'page_meta_title' ]          = $this->registry->output->formInput( 'page_meta_title', $page[ 'page_meta_title' ] );
        $page[ 'page_meta_keywords' ]       = $this->registry->output->formTextarea( 'page_meta_keywords', $page[ 'page_meta_keywords' ] );
        $page[ 'page_meta_description' ]    = $this->registry->output->formTextarea( 'page_meta_description', $page[ 'page_meta_description' ] );
        $page[ 'page_active' ]              = $this->registry->output->formYesNo( 'page_active', $page[ 'page_active' ] );
        $page[ 'page_in_footer' ]           = $this->registry->output->formYesNo( 'page_in_footer', $page[ 'page_in_footer' ] );
        $page[ 'page_in_header' ]           = $this->registry->output->formYesNo( 'page_in_header', $page[ 'page_in_header' ] );
        $page[ 'page_ipb_wrapper' ]         = $this->registry->output->formYesNo( 'page_ipb_wrapper', $page[ 'page_ipb_wrapper' ] );
        $page[ 'page_in_quick_nav' ]        = $this->registry->output->formYesNo( 'page_in_quick_nav', $page[ 'page_in_quick_nav' ] );
        $page[ 'page_custom_permission' ]   = $this->registry->output->formYesNo( 'page_custom_permission', $page[ 'page_custom_permission' ] );
        $page[ 'page_permission' ]          = $this->registry->output->formMultiDropdown( 'page_permission[]', $dropdown, explode( ",", trim( $page[ 'page_permission' ], ',' ) ), 5, 'page_permission' );
        
        $this->registry->output->html .= $this->html->pageForm( $page );
    }
    
    /**
     * Save Add/Edit state
     * 
     * @param string $type Type
     */
    private function saveForm( $type = 'add' )
    {
        /* Auth check */
        $this->registry->adminFunctions->checkSecurityKey();
        
        $page_id                  = intval( $this->request[ 'page_id' ] );
        $page_name                = trim( $this->request[ 'page_name' ] );
        $page_type                = trim( $this->request[ 'page_type' ] );
        $page_meta_title          = trim( $this->request[ 'page_meta_title' ] );
        $page_meta_keywords       = trim( $this->request[ 'page_meta_keywords' ] );
        $page_meta_description    = trim( $this->request[ 'page_meta_description' ] );
        $page_active              = intval( $this->request[ 'page_active' ] );
        $page_in_footer           = intval( $this->request[ 'page_in_footer' ] );
        $page_in_header           = intval( $this->request[ 'page_in_header' ] );
        $page_ipb_wrapper         = intval( $this->request[ 'page_ipb_wrapper' ] );
        $page_in_quick_nav        = intval( $this->request[ 'page_in_quick_nav' ] );
        $page_custom_permission   = intval( $this->request[ 'page_custom_permission' ] );
        $page_permission          = $this->request[ 'page_permission' ];
        
        /* Format description */
        $classToLoad = IPSLib::loadLibrary( IPS_ROOT_PATH . 'sources/classes/editor/composite.php', 'classes_editor_composite' );
        $editor = new $classToLoad();
        
        $editor->setAllowBbcode( true );
        $editor->setAllowSmilies( false );
        $editor->setAllowHtml( true );
        
        $page_content = $editor->process( $_POST [ 'page_content' ] );
        
        // Process editor
        IPSText::getTextClass( 'bbcode' )->parse_html = 1;
        IPSText::getTextClass( 'bbcode' )->parse_smilies = 0;
        IPSText::getTextClass( 'bbcode' )->parse_bbcode = 1;
        IPSText::getTextClass( 'bbcode' )->parsing_section = 'page_content';
        
        $page_content = IPSText::getTextClass( 'bbcode' )->preDbParse( $page_content );
        
        if( $page_name == '' )
        {
            $this->registry->output->showError( $this->lang->words[ 'sd33_p_page_bad_page_name' ], 'SD33P0003' );
        }
        
        if( $page_custom_permission && count( $page_permission ) == 0 )
        {
            $this->registry->output->showError( $this->lang->words[ 'sd33_p_page_bad_page_permission' ], 'SD33P0004' );
        }
        
        if( $page_content == '' )
        {
            $this->registry->output->showError( $this->lang->words[ 'sd33_p_page_bad_page_content' ], 'SD33P0005' );
        }
        
        $save = array(
            'page_name'                 => $page_name,
            'page_name_seo'	            => IPSText::makeSeoTitle( $page_name ),
        	'page_meta_title'           => $page_meta_title,
        	'page_meta_keywords'        => $page_meta_keywords,
        	'page_meta_description'	    => $page_meta_description,
        	'page_content'              => $page_content,
        	'page_active'               => $page_active,
        	'page_in_footer'	        => $page_in_footer,
        	'page_in_header'            => $page_in_header,
        	'page_ipb_wrapper'          => $page_ipb_wrapper,
        	'page_in_quick_nav'         => $page_in_quick_nav,
        	'page_type'                 => $page_type,
        	'page_custom_permission'	=> $page_custom_permission,
        	'page_permission'           => ( $page_custom_permission ? (count( $page_permission ) > 0 ? ( ',' . join( ',', $page_permission ) . ',' ) : '*' ) : '*' ) ,
        );
        
        if( $type == 'edit' ) 
        {
            $page = $this->sdpages->getPage( $page_id );
            
            if( !isset( $page[ 'page_id' ] ) ||  $page[ 'page_id' ] != $page_id )
            {
                $this->registry->output->showError( $this->lang->words[ 'sd33_p_page_bad_id' ], 'SD33P0006' );
            }
            
            $save[ 'page_edit_date' ] = time(); 
            
            $this->DB->update( 'sd_pages',  $save, 'page_id = ' . $page_id );
            
            $message = sprintf( $this->lang->words[ 'sd33_p_page_edited' ], $page_name, $page_id );
        }
        else 
        {
            $save[ 'page_add_date' ]  = time();
            $save[ 'page_edit_date' ] = time(); 
            $save[ 'page_position' ]  = $this->sdpages->getMaxPagePosition();
            
            $this->DB->insert( 'sd_pages', $save );
            
            $message = sprintf( $this->lang->words[ 'sd33_p_page_added' ], $page_name );
        }

        /* Set message & show categories, fudging over CID so the root is shown */
        $this->registry->getClass('adminFunctions')->saveAdminLog( $message );
        
        $this->registry->output->global_message = $message;
		$this->registry->output->silentRedirectWithMessage( "{$this->settings['base_url']}{$this->html->form_code}&amp;do=list" );
		return;
    }
    
    /**
     * Delete field
     */
    private function delPage()
    {
        $page_id = intval( $this->request[ 'page_id' ] );
        
        if( !$page_id )
        {
            $this->registry->output->showError( $this->lang->words[ 'sd33_p_page_bad_id' ], 'SD33P0007' );
        }
        
        $this->DB->delete( "sd_pages", "page_id={$page_id}" );
        
        $message = sprintf( $this->lang->words[ 'sd33_p_page_deleted' ], $page_id );
        
        /* Set message & show categories, fudging over CID so the root is shown */
        $this->registry->getClass('adminFunctions')->saveAdminLog( $message );
        
        $this->registry->output->global_message = $message;
		$this->registry->output->silentRedirectWithMessage( "{$this->settings['base_url']}{$this->html->form_code}&amp;do=list" );
		return;
    }
    
	/**
	 * Reorder field
	 */
    private function reorderPage()
	{
		//-----------------------------------------
		// INIT
		//-----------------------------------------

		$classToLoad = IPSLib::loadLibrary( IPS_KERNEL_PATH . 'classAjax.php', 'classAjax' );
		$ajax		 = new $classToLoad();
		
		//-----------------------------------------
		// Checks...
		//-----------------------------------------

		if( $this->registry->adminFunctions->checkSecurityKey( $this->request['md5check'], true ) === false )
		{
			$ajax->returnString( $this->lang->words[ 'no_permission' ] );
			exit;
		}
 		
 		//-----------------------------------------
 		// Save new position
 		//-----------------------------------------

 		$position	= 1;
 		
 		if( is_array( $this->request[ 'pages' ] ) AND count( $this->request[ 'pages' ] ) )
 		{
 			foreach( $this->request[ 'pages' ] as $this_id )
 			{
 				$this->DB->update( 'sd_pages', array( 'page_position' => $position ), 'page_id=' . $this_id );
 				
 				$position++;
 			}
 		}

 		$ajax->returnString( 'OK' );
 		exit();
	}
}
?>