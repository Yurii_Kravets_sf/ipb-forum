<?php
/**
 * SolutionrDEVs Application
 * (SD33) Pages
 *
 * @author      Dawid Baruch <dawid.baruch@solutiondevs.pl>
 * @copyright   (c) 2005 - 2012 SolutionDEVs
 * @package     SolutionDEVs Apps
 * @subpackage  PHP
 * @link        http://www.solutiondevs.pl
 * @link        http://www.ipsbeyond.pl
 * @version     1.0.0 
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.<br />Author: Dawid Baruch <a href='http://www.solutiondevs.pl'><strong>SolutionDEVs.pl</strong></a>";
	exit();
}

class admin_sdpages_overview_overview extends ipsCommand 
{
    /**
     * Skin object
     *
     * @access	private
     * @var	object			Skin templates
     */
    private $html;

    /**
     * Shortcut for url
     *
     * @access	private
     * @var	string			URL shortcut
     */
    private $form_code;

    /**
     * Shortcut for url (javascript)
     *
     * @access	private
     * @var	string			JS URL shortcut
     */
    private $form_code_js;

    /**
     * class Public Permissions
     *
     * @var classPublicPermissions
     */
    private $permissions;

    /**
     * Main application classs
     *
     * @var sdpages
     */
    private $sdpages;
    
    
    /**
     * Main class entry point
     *
     * @access	public
     * @param	object		ipsRegistry reference
     * @return	void		[Outputs to screen]
     */
    public function doExecute( ipsRegistry $registry ) 
    {
        $this->html = $this->registry->output->loadTemplate( 'cp_skin_sdpages' );

        $this->form_code    = 'module=overview&amp;section=overview';
        $this->form_code_js = 'module=overview&section=overview';

        ipsRegistry::getClass('class_localization')->loadLanguageFile( array( 'admin_lang' ), 'sdpages' );
        ipsRegistry::getClass('class_localization')->loadLanguageFile( array( 'admin_tools' ), 'core' );
        
        require_once( IPS_ROOT_PATH . 'sources/classes/class_public_permissions.php' );
        $this->permissions = new classPublicPermissions( ipsRegistry::instance() );

        require_once( IPSLib::getAppDir( 'sdpages' ) . '/sources/classSdPages.php' );
        $this->sdpages = new sdpages( $this->registry );

        switch( $this->request['do'] ) 
        {
            case 'settings':
                $this->registry->getClass('class_permissions')->checkPermissionAutoMsg( 'sdpages_settings' );
                $this->settings();
            break;
            
            case 'overview':
            default:
                $this->registry->getClass('class_permissions')->checkPermissionAutoMsg( 'sdpages_overview' );
                $this->overview();
            break;
        }

        //-----------------------------------------
        // Output
        //-----------------------------------------
        
        $this->registry->output->html .= $this->html->footer();
        $this->registry->output->html_main .= $this->registry->output->global_template->global_frame_wrapper();
        $this->registry->output->sendOutput();
    }

    /**
     * Przegląd
     */
    private function overview() 
    {
        $statistics = array(
            'amount_pages'       => $this->sdpages->getAmountOfPages(),
        );
        
        $this->registry->output->html .= $this->html->overview( $statistics );
    }

	/**
     * Ustawienia
     */     
    private function settings()
    {
        $this->html->form_code    = $this->form_code	= 'module=overview&amp;section=overview&amp;do=settings';
		$this->html->form_code_js = $this->form_code_js	= 'module=overview&section=overview&do=settings';
		
		/* Get settings library */
		$classToLoad = IPSLib::loadActionOverloader( IPSLib::getAppDir('core').'/modules_admin/settings/settings.php', 'admin_core_settings_settings' );
		$settings    = new $classToLoad();
		$settings->makeRegistryShortcuts( $this->registry );
		
		ipsRegistry::getClass('class_localization')->loadLanguageFile( array( 'admin_tools' ), 'core' );
		
		$settings->html			= $this->registry->output->loadTemplate( 'cp_skin_settings', 'core' );		
		$settings->form_code	= $settings->html->form_code    = 'module=settings&amp;section=settings&amp;';
		$settings->form_code_js	= $settings->html->form_code_js = 'module=settings&section=settings&';

		$this->request['conf_title_keyword'] = 'sd33_p';
		$settings->return_after_save         = $this->settings['base_url'] . $this->form_code;
		$settings->_viewSettings();
		
		$this->registry->getClass('output')->html_main .= $this->registry->getClass('output')->global_template->global_frame_wrapper();
		$this->registry->getClass('output')->sendOutput();		

		$this->form_code    = 'module=overview&amp;section=overview';
        $this->form_code_js = 'module=overview&section=overview';
    }
}
?>