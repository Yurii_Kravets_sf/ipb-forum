<?php
/**
 * SolutionrDEVs Application
 * (SD33) Pages
 *
 * @author      Dawid Baruch <dawid.baruch@solutiondevs.pl>
 * @copyright   (c) 2005 - 2012 SolutionDEVs
 * @package     SolutionDEVs Apps
 * @subpackage  PHP
 * @link        http://www.solutiondevs.pl
 * @link        http://www.ipsbeyond.pl
 * @version     1.0.0 
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.<br />Author: Dawid Baruch <a href='http://www.solutiondevs.pl'><strong>SolutionDEVs.pl</strong></a>";
	exit();
}

class sdpages
{
    public static $itemsPerPage = 20;
    
	/**
     * Registry
     * 
     * @var ipsRegistry
     */
    private $registry;
    
    /**
     * DB handle
     * 
     * @var db_driver_mysql
     */
	private $DB;
	
	/**
	 * Settings
	 * 
	 * @var array
	 */
	private $settings;
	
	/**
	 * Request
	 *
	 * @var array
	 */
	private $request;
	
	/**
	 * Language
	 *
	 * @var class_localization
	 */
	private $lang;
	
	/**
	 * Member
	 *
	 * @var ips_MemberRegistry
	 */
	private $member;
	
	/**
	 * Member Data
	 *
	 * @var array
	 */
	private $memberData;
	
	/**
	 * Cache class
	 *
	 * @var ips_CacheRegistry
	 */
	private $cache;
	
	/**
	 * Cache
	 *
	 * @var array
	 */
	private $caches;
	
	/**
	 * Editor
	 *
	 * @var classes_editor_composite
	 */
	private $editor;
	
	/**
	 * Setup registry classes
	 *
	 * @access	public
	 * @param	ipsRegistry	$registry
	 * @return	void
	 */
	public function __construct( ipsRegistry $registry )
	{
		/* Make registry objects */
		$this->registry   =  $registry;
		$this->DB         =  $this->registry->DB();
		$this->settings   =& $this->registry->fetchSettings();
		$this->request    =& $this->registry->fetchRequest();
		$this->lang       =  $this->registry->getClass('class_localization');
		$this->member     =  $this->registry->member();
		$this->memberData =& $this->registry->member()->fetchMemberData();
		$this->cache      =  $this->registry->cache();
        $this->caches     =& $this->cache->fetchCaches();
	}
	
	/**
	 * Return fields amount
	 * 
	 * @return int
	 */
	public function getAmountOfPages()
	{
	    $amount = $this->DB->buildAndFetch( array( 
                                'select' => 'COUNT(*) AS amount',
                                'from'   => 'sd_pages',
                        )   );
                        
        return isset( $amount[ 'amount' ] ) ? $amount[ 'amount' ] : 0;
	}
	
	/**
	 * Get fields list
	 * 
	 * @param int $start
	 * @return array
	 */
	public function getPagesList( $start )
	{
	    $results = array();
	    
	    $this->DB->build( array( 
                                'select' => '*',
                                'from'   => 'sd_pages',
	                            'order'	 => 'page_position ASC',
                            	'limit'	 => array( $start, self::$itemsPerPage )
                        )   );
                        
        $res = $this->DB->execute();
        
        while( $r = $this->DB->fetch( $res ) )
        {
            $r[ 'page_active_image' ]          = $r[ 'page_active' ] ? 'aff_tick.png' : 'aff_cross.png';
            $r[ 'page_in_footer_image' ]       = $r[ 'page_in_footer' ] ? 'aff_tick.png' : 'aff_cross.png';
            $r[ 'page_in_header_image' ]       = $r[ 'page_in_header' ] ? 'aff_tick.png' : 'aff_cross.png';
            $r[ 'page_ipb_wrapper_image' ]     = $r[ 'page_ipb_wrapper' ] ? 'aff_tick.png' : 'aff_cross.png';
            $r[ 'page_in_quick_nav_image' ]    = $r[ 'page_in_quick_nav' ] ? 'aff_tick.png' : 'aff_cross.png';
            
            $results[] = $r;
        }
        
        return $results;
	}
	
	/**
	 * Get page info
	 * 
	 * @param ing $page_id
	 */
	public function getPage( $page_id )
	{
	    $pageInfo = $this->DB->buildAndFetch( array( 
                                'select' => '*',
                                'from'   => 'sd_pages',
	                            'where'	 => 'page_id=' . intval( $page_id )
                        )   );
                        
        return ( ( is_array( $pageInfo ) && count( $pageInfo ) ) ? $pageInfo : array() );
	}
	
	/**
	 * Return max position
	 */
	public function getMaxPagePosition()
	{
	    $pageInfo = $this->DB->buildAndFetch( array( 
                                'select' => 'max(page_position) as maxvalue',
                                'from'   => 'sd_pages',
                        )   );
                        
        return isset( $pageInfo[ 'maxvalue' ] ) ? $pageInfo[ 'maxvalue' ] + 1 : 1;
	}
	
	/**
	 * Return page for footer
	 */
	public function getPagesInFooter()
	{
	    $this->DB->build( array( 
                                'select' => 'page_id, page_name, page_name_seo',
                                'from'   => 'sd_pages',
	                            'order'	 => 'page_position ASC',
	                            'where'	 => "page_active=1 AND page_in_footer=1 AND (page_permission='*' OR (page_custom_permission=1 AND page_permission LIKE '%,{$this->memberData[ 'member_group_id' ]},%'))",
                        )   );
                        
        $res = $this->DB->execute();
        
        $results = array();
        
        while( $r = $this->DB->fetch( $res ) )
        {
            $r[ 'page_name' ] = ( $r[ 'page_meta_title' ] != '' ? $r[ 'page_meta_title' ] : $r[ 'page_name' ] );
            
            $results[] = $r;
        }
        
        return $results;
	}
	
	/**
	 * Return page for quick nav
	 */
	public function getPagesInQuickNav()
	{
	    $this->DB->build( array( 
                                'select' => 'page_id, page_name, page_name_seo',
                                'from'   => 'sd_pages',
	                            'order'	 => 'page_position ASC',
	                            'where'	 => "page_active=1 AND page_in_quick_nav=1 AND (page_permission='*' OR (page_custom_permission=1 AND page_permission LIKE '%,{$this->memberData[ 'member_group_id' ]},%'))",
                        )   );
                        
        $res = $this->DB->execute();
        
        $results = array();
        
        while( $r = $this->DB->fetch( $res ) )
        {
            $r[ 'page_name' ] = ( $r[ 'page_meta_title' ] != '' ? $r[ 'page_meta_title' ] : $r[ 'page_name' ] );
            
            $results[] = $r;
        }
        
        return $results;
	}
	
	/**
	 * Return page for header
	 */
	public function getPagesInHeader()
	{
	    $this->DB->build( array( 
                                'select' => 'page_id, page_name, page_name_seo',
                                'from'   => 'sd_pages',
	                            'order'	 => 'page_position ASC',
	                            'where'	 => "page_active=1 AND page_in_header=1 AND (page_permission='*' OR (page_custom_permission=1 AND page_permission LIKE '%,{$this->memberData[ 'member_group_id' ]},%'))",
                        )   );
                        
        $res = $this->DB->execute();
        
        $results = array();
        
        while( $r = $this->DB->fetch( $res ) )
        {
            $r[ 'page_name' ] = ( $r[ 'page_meta_title' ] != '' ? $r[ 'page_meta_title' ] : $r[ 'page_name' ] );
            
            $results[] = $r;
        }
        
        return $results;
	}
	
	/**
	 * Return list of pages
	 * 
	 * @return array
	 */
	public function getPublicPagesList()
	{
	    $this->DB->build( array( 
                                'select' => 'page_id, page_name, page_name_seo, page_content',
                                'from'   => 'sd_pages',
	                            'order'	 => 'page_position ASC',
	                            'where'	 => "page_active=1 AND (page_permission='*' OR (page_custom_permission=1 AND page_permission LIKE '%,{$this->memberData[ 'member_group_id' ]},%'))",
                        )   );
                        
	    $res = $this->DB->execute();
        
        $results = array();
        
        IPSText::getTextClass( 'bbcode' )->parse_smilies			= 1;
		IPSText::getTextClass( 'bbcode' )->parse_html				= 1;
		IPSText::getTextClass( 'bbcode' )->parse_nl2br				= 1;
		IPSText::getTextClass( 'bbcode' )->parse_bbcode				= 1;
		IPSText::getTextClass( 'bbcode' )->parsing_section			= 'page_content';
        
        while( $r = $this->DB->fetch( $res ) )
        {
            $r[ 'page_name' ] = ( $r[ 'page_meta_title' ] != '' ? $r[ 'page_meta_title' ] : $r[ 'page_name' ] );
            
            $r[ 'page_content' ]	 = IPSText::getTextClass( 'bbcode' )->preDisplayParse( $r[ 'page_content' ] );
            $r[ 'page_content' ]     = IPSText::truncate( IPSText::getTextClass( 'bbcode' )->stripAllTags( strip_tags( $r[ 'page_content' ], '<br>' ) ), 500 );
            
            $results[] = $r;
        }
        
        return $results;
	}
	
	/**
	 * Return list of pages
	 * 
	 * @return array
	 */
	public function getPagesForRss()
	{
	    $this->DB->build( array( 
                                'select' => 'page_id, page_name, page_name_seo, page_content, page_edit_date',
                                'from'   => 'sd_pages',
	                            'order'	 => 'page_edit_date DESC',
	                            'where'	 => "page_active=1 AND (page_permission='*' OR (page_custom_permission=1 AND page_permission LIKE '%,{$this->settings[ 'guest_group' ]},%'))",
                        )   );
                        
	    $res = $this->DB->execute();
        
        $results = array();
        
        IPSText::getTextClass( 'bbcode' )->parse_smilies			= 1;
		IPSText::getTextClass( 'bbcode' )->parse_html				= 1;
		IPSText::getTextClass( 'bbcode' )->parse_nl2br				= 1;
		IPSText::getTextClass( 'bbcode' )->parse_bbcode				= 1;
		IPSText::getTextClass( 'bbcode' )->parsing_section			= 'page_content';
        
        while( $r = $this->DB->fetch( $res ) )
        {
            $r[ 'page_name' ] = ( $r[ 'page_meta_title' ] != '' ? $r[ 'page_meta_title' ] : $r[ 'page_name' ] );
            
            $r[ 'page_content' ]	 = IPSText::getTextClass( 'bbcode' )->preDisplayParse( $r[ 'page_content' ] );
            $r[ 'page_content' ]     = IPSText::truncate( IPSText::getTextClass( 'bbcode' )->stripAllTags( strip_tags( $r[ 'page_content' ], '<br>' ) ), 500 );
            
            $results[] = $r;
        }
        
        return $results;
	}
	
	/**
	 * Return page
	 * 
	 * @return array
	 */
	public function getPublicPage( $page_id)
	{
	    $pageInfo = $this->DB->buildAndFetch( array( 
                                'select' => '*',
                                'from'   => 'sd_pages',
	                            'order'	 => 'page_position ASC',
	                            'where'	 => "page_id = {$page_id} AND page_active=1 AND (page_permission='*' OR (page_custom_permission=1 AND page_permission LIKE '%,{$this->memberData[ 'member_group_id' ]},%'))",
                        )   );
                        
        return count( $pageInfo ) ? $pageInfo : false;
	}
}
?>