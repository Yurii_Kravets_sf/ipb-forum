<?php
/**
 * SolutionrDEVs Application
 * (SD33) Pages
 *
 * @author      Dawid Baruch <dawid.baruch@solutiondevs.pl>
 * @copyright   (c) 2005 - 2012 SolutionDEVs
 * @package     SolutionDEVs Apps
 * @subpackage  PHP
 * @link        http://www.solutiondevs.pl
 * @link        http://www.ipsbeyond.pl
 * @version     1.0.0 
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.<br />Author: Dawid Baruch <a href='http://www.solutiondevs.pl'><strong>SolutionDEVs.pl</strong></a>";
	exit();
}

class public_sdpages_pages_pages extends ipsCommand 
{
    /**
     * Basic template, replaced as needed
     *
     * @access  protected
     * @var     string Basic skin template to replace
     */
    protected $template;
    
    /**
     * Main application classs
     *
     * @var sdpages
     */
    private $sdpages;
    
    
    /**
     * Main class entry point
     *
     * @access	public
     * @param	object		ipsRegistry reference
     * @return	void		[Outputs to screen]
     */
    public function doExecute( ipsRegistry $registry ) 
    {
        ipsRegistry::getClass('class_localization')->loadLanguageFile( array( 'public_lang' ), 'sdpages' );

        require_once( IPSLib::getAppDir( 'sdpages' ) . '/sources/classSdPages.php' );
        $this->sdpages = new sdpages( $this->registry );
        
        if( !$this->settings[ 'sd33_p_turnon' ] && !in_array( $this->memberData[ 'member_group_id' ], explode( ',', $this->settings[ 'sd33_p_offline_groups' ] ) ) )
        {
            IPSText::getTextClass( 'bbcode' )->parse_smilies			= 1;
    		IPSText::getTextClass( 'bbcode' )->parse_html				= 0;
    		IPSText::getTextClass( 'bbcode' )->parse_nl2br				= 1;
    		IPSText::getTextClass( 'bbcode' )->parse_bbcode				= 1;
    		IPSText::getTextClass( 'bbcode' )->parsing_section			= 'sd33_p_offline_message';
        
            $this->settings[ 'sd33_p_offline_message' ] = IPSText::getTextClass( 'bbcode' )->preDisplayParse( $this->settings[ 'sd33_p_offline_message' ] );
            
            $this->registry->output->addNavigation( $this->lang->words[ 'sd33_p_pages_list_title' ], 'app=sdpages', "false", "app=sdpages" );
		    $this->registry->output->setTitle( $this->lang->words[ 'sd33_p_pages_list_title_offline' ] );
            
            $this->template .= $this->registry->output->getTemplate( 'sdpages' )->displayOffline( $this->settings[ 'sd33_p_offline_message' ] );
            $this->_do_output();
        }
        
        if( $this->settings[ 'sd33_p_turnon' ] && !in_array( $this->memberData[ 'member_group_id' ], explode( ',', $this->settings[ 'sd33_p_groups' ] ) ) )
        {
            $this->registry->output->showError( $this->lang->words[ 'sd33_p_no_permission' ], 'SD33P0008' );
        }
        
        $this->showPagesList();

        //-----------------------------------------
        // Output
        //-----------------------------------------
        
        $this->_do_output();
    }

    /**
     * Internal do output method.  Extend class and overwrite method if you need to modify this functionality.
     *
     * @access  protected
     * @return  void
     */
    protected function _do_output()
    {
        $year = date( "Y" );
        
        $this->template .= <<<HTML
		<br class="clear" /><br />
        <div id="pageFooter">
            Перевод {$this->caches['app_cache']['sdpages']['app_version']} &copy; 2005 - {$year} &nbsp;<a style='text-decoration:none' href='http://www.ipbzona.ru' title='Стили, русские хуки и компоненты'><span style="text-decoration:none;" class="ipsBadge ipsBadge_green">IpbZona</span></a> &amp; <a style='text-decoration:none' href='http://www.ipbzona.ru' title='IP.Board техническая поддержка'><span style="text-decoration:none;" class="ipsBadge ipsBadge_green">IP.Board техническая поддержка</span></a>
        </div>
        <br class="clear" />
HTML;

        //-----------------------------------------
        // Pass to print...
        //-----------------------------------------
        
        $this->registry->output->addContent( $this->template );
        $this->registry->output->sendOutput();

        exit();
    }
    
    private function showPagesList()
    {
        // Add Navigation
		$this->registry->output->addNavigation( $this->lang->words[ 'sd33_p_pages_list_title' ], 'app=sdpages', "false", "app=sdpages" );
		$this->registry->output->setTitle( $this->lang->words[ 'sd33_p_pages_list_title' ] );
		
        $this->template .= $this->registry->output->getTemplate( 'sdpages' )->showPages( $this->sdpages->getPublicPagesList() );
    }
}
?>