<?php
//-----------------------------------------------
// (DP34) Pages
//-----------------------------------------------
//-----------------------------------------------
// ACP Skin Content
//-----------------------------------------------
// Author: DawPi
// Site: http://www.ipslink.pl
// Written on: 11 / 07 / 2009
// Updated on: 17 / 12 / 2012
//-----------------------------------------------
// Copyright (C) 2009 - 2012 DawPi
// All Rights Reserved
//-----------------------------------------------  

class cp_cp_pages extends output
{

/**
 * Prevent our main destructor being called by this class
 *
 * @access	public
 * @return	void
 */
public function __destruct()
{
}


public function pagesListView( $pages, $pagination ) {


$IPBHTML = "";
//--starthtml--//

$IPBHTML .= <<<HTML
<div class='section_title'>
	<h2>{$this->lang->words['pageOverview']}</h2>
	<ul class='context_menu'>
		<li>
			<a href='{$this->settings['base_url']}{$this->form_code}&amp;do=addPage'>
				<img src='{$this->settings['skin_acp_url']}/images/icons/add.png' alt='' />
				{$this->lang->words['addNew']}
			</a>
		</li>
HTML;

if( count( $pages ) AND is_array( $pages ) )
{
		$IPBHTML .= <<<HTML
		<li>
			<a href='{$this->settings['base_url']}{$this->form_code}&amp;do=updateCache' title='{$this->lang->words['update_pages_cache']}'>
				<img src='{$this->settings[ 'skin_app_url']}/images/arrow-circle-double-135.png' alt='' />
				{$this->lang->words['update_pages_cache']}
			</a>
		</li>
		<li>
			<a href='#' onclick='acp.confirmDelete("{$this->settings['base_url']}{$this->form_code}&amp;do=removeAll");'>
				<img src='{$this->settings['skin_acp_url']}/images/icons/delete.png' alt='' />
				{$this->lang->words['removeAll']}
			</a>
		</li>
HTML;
}		
		$IPBHTML .= <<<HTML
	</ul>
</div>
<div class="acp-box">			
HTML;

if( count( $pages ) AND is_array( $pages ) )
{  
		$IPBHTML .= <<<HTML
	<h3>{$this->lang->words['pages']}</h3>
		<table class='ipsTable' id='reordable_pages'>
			<tr>
			    <th style='width: 3%; text-align:center;'>&nbsp;</th>			
			    <th style='width: 5%; text-align:center;'>{$this->lang->words['id']}</th>		
	            <th style='width: 37%; text-align:left;'>{$this->lang->words['pageTitle']}</th>			
				<th style='width: 20%; text-align:center;'>{$this->lang->words['dateAdded']}</th>
				<th style='width: 20%; text-align:center;'>{$this->lang->words['dateUpdated']}</th>			
				<th style='width: 5%; text-align:center;'>{$this->lang->words['pageActive']}</th>
				<th style='width: 10%; text-align:center;'></th>			
			</tr>				
HTML;
	foreach( $pages as $page )
	{
       $img = ( $page['cp_active'] ) ? 'accept.png' : 'cross.png';
       
		$IPBHTML .= <<<HTML
			<tr class='ipsControlRow isDraggable' id='pages_{$page['cp_id']}'>
				<td style='text-align:center;'><div class='draghandle'><img src='{$this->settings['skin_acp_url']}/_newimages/drag_icon.png' alt='drag' /></div></td>		
			    <td align='center'>{$page['cp_id']}</td>
				<td><strong><a href="{$this->settings['base_url']}{$this->form_code}&amp;do=editPage&amp;page_id={$page['cp_id']}&amp;st={$this->request['st']}" title='{$this->lang->words['edit']}{$this->lang->words['page']}'><span class='larger_text'>{$page['cp_title']}</span></a></strong> <a href="{$this->settings['board_url']}/index.php?app=cp&amp;do=show&amp;pageId={$page['cp_id']}" title='{$this->lang->words['seePage']}' target='_blank'><img src='{$this->settings[ 'skin_app_url']}/images/arrow_right.png' border='0' class='ipbmenu' /></a> <br /> <span class='desctext'>{$page['cp_page_desc']}</span> </td>
				<td style='text-align:center;'>{$this->registry->getClass( 'class_localization')->getDate( $page['cp_dateadded'], 'LONG', 1 )}</td>
				<td style='text-align:center;'>{$this->registry->getClass( 'class_localization')->getDate( $page['cp_updated'], 'LONG', 1 )}</td>
				<td align='center' nowrap='nowrap'><strong><a href='{$this->settings['base_url']}{$this->form_code}&amp;do=togglePage&amp;page_id={$page['cp_id']}&amp;st={$this->request['st']}' title='{$this->lang->words['toggle']}'><img src='{$this->settings['skin_acp_url']}/_newimages/icons/{$img}' border='0' class='ipbmenu' /></a></strong></td>
				<td style='text-align:center;'>
					<ul class='ipsControlStrip'>
						<li class='i_edit'><a href='{$this->settings['base_url']}{$this->form_code}&amp;do=editPage&amp;page_id={$page['cp_id']}&amp;st={$this->request['st']}'>{$this->lang->words['edit']}{$this->lang->words['page']}</a></li>
						<li class='i_delete'><a href='{$this->settings['base_url']}&amp;{$this->form_code}&amp;do=removePage&amp;page_id={$page['cp_id']}&amp;st={$this->request['st']}' onclick='return confirm("{$this->lang->words['are_you_sure_to_remove']}")' title='{$this->lang->words['remove_referred_member']}'>{$this->lang->words['remove']}{$this->lang->words['page']}</a></li>
					</ul>
				</td>					
			</tr>
HTML;
	}
	$IPBHTML .= <<<HTML
        </table>
HTML;
}
else
{
	$IPBHTML .= <<<HTML
		<table class='ipsTable'>
        	<tr>
				<td class='no_messages'>{$this->lang->words['noPages1']}</td>
			</tr>
        </table>
HTML;
}

$IPBHTML .= <<<HTML
</div>
<div style='padding-top:5px;padding-bottom:15px;'>
{$pagination}
</div>

<script type='text/javascript'>
	jQ("#reordable_pages").ipsSortable( 'table', {url: "{$this->settings['base_url']}&app=cp&module=pagemanager&section=pages&do=reorder&md5check={$this->registry->adminFunctions->getSecurityKey()}".replace( /&amp;/g, '&' )});
</script>
HTML;

//--endhtml--//
return $IPBHTML;
}

/**
 * View the form to add/edit a moderator
 *
 *
**/
  
public function formView( $form, $button, $typeAfter, $st ) {

$IPBHTML = "";
//--starthtml--//

$IPBHTML .= <<<HTML
<div class='section_title'>
	<h2>{$form['formDo']}</h2>
</div>
HTML;

$IPBHTML .= <<<HTML
<form action='{$this->settings['base_url']}{$this->form_code}&amp;do=checkPage&amp;type={$typeAfter}&amp;page_id={$form['cp_id']}&amp;st={$st}' method='post' name='theAdminForm'  id='theAdminForm'>
	<input type='hidden' name='_admin_auth_key' value='{$this->registry->adminFunctions->getSecurityKey()}' />	
	<div class='acp-box'>
		<h3 >{$form['formDoDesc']}</h3>		
			<table class='ipsTable double_pad'>
			<tr>
				<th colspan='2'>{$this->lang->words['main_settings']}</th>
			</tr>
			<tr>
				<td class='field_title'><strong class='title'>{$this->lang->words['pageTitle']}</strong></td>
				<td class='field_field'>
					{$form['cp_title']}
				</td>	
			</tr>
			<tr>
				<td class='field_title'><strong class='title'>{$this->lang->words['pageDesc']}</strong></td>
				<td class='field_field'>
					{$form['cp_page_desc']}
				</td>	
			</tr>
			<tr>
				<th colspan='2'>{$this->lang->words['settings']}</th>
			</tr>			
			<tr>
				<td class='field_title'><strong class='title'>{$this->lang->words['pageActive']}</strong></td>
				<td class='field_field'>
					{$form['cp_active']}
				</td>	
			</tr>
			<tr>
				<td class='field_title'><strong class='title'>{$this->lang->words['allowedGroups']}</strong></td>
				<td class='field_field'>
					{$form['cp_groups']}<br />
					<span class='desctext'>{$this->lang->words['allowedGroupsDesc']}</span>
				</td>	
			</tr>
			<tr>
				<th colspan='2'>{$this->lang->words['display_settings']}</th>
			</tr>
			<tr>
				<td class='field_title'><strong class='title'>{$this->lang->words['header_display']}</strong></td>
				<td class='field_field'>
					{$form['cp_show_in_top']}<br />
					<span class='desctext'>{$this->lang->words['header_display_desc']}</span>
				</td>	
			</tr>							
			<tr>
				<td class='field_title'><strong class='title'>{$this->lang->words['is_js_page']}</strong></td>
				<td class='field_field'>
					{$form['cp_is_js']}<br />
					<span class='desctext'>{$this->lang->words['is_js_page_desc']}</span>
				</td>	
			</tr>
			<tr>
				<th colspan='2'>{$this->lang->words['meta_settings']}</th>
			</tr>					
			<tr>
				<td class='field_title'><strong class='title'>{$this->lang->words['meta_keywords']}</strong></td>
				<td class='field_field'>
					{$form['cp_keywords']}
				</td>	
			</tr>
			<tr>
				<td class='field_title'><strong class='title'>{$this->lang->words['meta_desc']}</strong></td>
				<td class='field_field'>
					{$form['cp_description']}
				</td>	
			</tr>				
			<tr>
				<th colspan='2'>{$this->lang->words['pageTxt']}</th>
			</tr>					
			<tr>
				<td class='field_field' colspan='2'>
					{$form['cp_text']}<br />
				</td>	
			</tr>								
			</table>
						
		<div class='acp-actionbar'>
				<input type='submit' value='{$button}' class='button primary' accesskey='s'> 
HTML;

if( isset( $this->request['page_id'] ) && $this->request['page_id'] )
{
$IPBHTML .= <<<HTML
				<input type='submit' name='save_and_reload' value=' {$this->lang->words['button__reload']} ' class="button primary" />
HTML;
}

$IPBHTML .= <<<HTML
		</div>
	</div>
</form>
HTML;

//--endhtml--//
return $IPBHTML;
}

}// End of class