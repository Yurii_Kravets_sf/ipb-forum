<?php

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.";
	exit();
}

class furlRedirect_cp
{	
	/**
	 * Key type
	 *
	 * @var			string
	 * @access	private
	 */
	private $_type;
	
	/**
	 * Key ID
	 *
	 * @var			integer
	 * @access	private
	 */
	private $_id;
	
	/**
	 * Constructor - hook us up with the registry
	 *
	 * @param		object		$registry		ipsRegistry instance
	 * @access	public
	 */
	public function __construct( ipsRegistry $registry )
	{
		$this->registry =  $registry;
		$this->DB       =  $registry->DB();
		$this->settings =& $registry->fetchSettings();
	}

	/**
	 * Set the key ID
	 *
	 * @param		string		$name		Name of key to set
	 * @param		integer		$value	Value of key ID to set
	 * @return	void
	 * @access	public
	 */
	public function setKey( $name, $value )
	{
		$this->_type = $name;
		$this->_id   = $value;
	}
	
	/**
	 * Set up the key by URI
	 *
	 * @param		string		$uri		URI to set up from
	 * @return	boolean		True on match
	 * @access	public
	 */
	public function setKeyByUri( $uri )
	{
		if ( IN_ACP )
		{
			return FALSE;
		}
		
		$uri = str_replace( '&amp;', '&', $uri );

		if ( strstr( $uri, '?' ) )
		{
			list( $_chaff, $uri ) = explode( '?', $uri );
		}
		
		if ( $uri == 'app=cp' )
		{
			$this->setKey( 'app', 'cp' );
			return TRUE;			
		}
		else
		{
			foreach ( explode( '&', $uri ) as $bits )
			{
				list( $k, $v ) = explode( '=', $bits );
				
				if ( $k )
				{
					if ( $k == 'page' )
					{
						$this->setKey( 'page', intval( $v ) );
						return TRUE;
					}
				}
			}
		}
		
		return FALSE;
	}
	
	/**
	 * Return the SEO title
	 *
	 * @param		void
	 * @return	string		The SEO friendly name
	 * @access	public
	 */
	public function fetchSeoTitle()
	{
		switch ( $this->_type )
		{
			case 'app':
				return 'cp';
				break;
		}
		
		return false;
	}
}