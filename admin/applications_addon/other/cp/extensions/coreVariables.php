<?php

//-----------------------------------------------
// (DP32) Pages
//-----------------------------------------------
//-----------------------------------------------
// Core Variables
//-----------------------------------------------
// Author: DawPi
// Site: http://www.ipslink.pl
// Written on: 27 / 07 / 2011
//-----------------------------------------------
// Copyright (C) 2011 DawPi
// All Rights Reserved
//-----------------------------------------------  

/**
* Array for holding reset information
*
* Populate the $_RESET array and ipsRegistry will do the rest
*/

$_RESET = array();

###### Redirect requests... ######
if ( $_REQUEST['autocom'] == 'custompages' )
{
	$_RESET['app'] = 'cp';
}

if ( $_REQUEST['app'] == 'custompages' )
{
	$_RESET['app'] = 'cp';
}

if ( !isset( $_REQUEST['module'] ) && ( isset( $_REQUEST['app'] ) && $_REQUEST['app'] == 'cp' ) )
{
	$_RESET['module'] = 'view';
}

/* Shortcut links */
if ( isset($_REQUEST['pageId']) && intval($_REQUEST['pageId']) > 0 )
{
	$_RESET['app']     = 'cp';
	$_RESET['module']  = 'view';
	$_RESET['section'] = 'display';
	$_RESET['pageId']  = intval($_REQUEST['pageId']);
}

/* Caches */

$_LOAD = array();

$_LOAD['cp_pages']	= 1;
		   
$CACHE['cp_pages'] 	= array( 
									'array'				=> 1,
								   	'allow_unload'		=> 0,
								   	'default_load'		=> 1,
								   	'recache_file'		=> IPSLib::getAppDir( 'cp' ) . '/sources/classes/library.php',
								   	'recache_class'		=> 'cpLibrary',
								   	'recache_function'	=> 'updateCache'
								);			