<?php

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.";
	exit();
}

$_SEOTEMPLATES = array(
	'dp3_cp_show_page'    => array( 'app'	  => 'cp',
							  'allowRedirect' => 1,
							  'out'           => array( '/app=cp(?:&|&amp;)do=show(?:&|&amp;)pageId=(.+?)(&|$|\#)/i', 'cp/$1-#{__title__}/$2' ),
							  'in'            => array( 'regex'   => "#/cp/(\d+?)-#i",
			'matches'		=> array( 
				array( 'app'		, 'cp' ),
				array( 'module'	    , 'view' ),	
				array( 'section'	, 'display' ),									
				array( 'do'	        , 'show' ),									
				array( 'pageId'		, "$1" )
									)	
								),
						),		
								
																					
	'app=cp'		=> array( 
						'app'			=> 'cp',
						'allowRedirect' => 1,
						'out'			=> array( '#app=cp#i', 'cp/' ),
						'in'			=> array( 
													'regex'		=> "#/cp(/|$|\?)#i",
													'matches'	=> array( array( 'app', 'cp' ) )
												) 
									),																																				
);