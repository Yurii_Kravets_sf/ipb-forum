<?php

$TABLE[] = "ALTER TABLE dp23_cuspg ADD cp_keywords text NOT NULL";
$TABLE[] = "ALTER TABLE dp23_cuspg ADD cp_description text NOT NULL";
$TABLE[] = "ALTER TABLE dp23_cuspg ADD cp_page_desc text NOT NULL";
$TABLE[] = "ALTER TABLE dp23_cuspg ADD cp_allow_html tinyint(1) NOT NULL DEFAULT '0'";
$TABLE[] = "ALTER TABLE dp23_cuspg ADD cp_allow_bbcode tinyint(1) NOT NULL DEFAULT '1'";

$SQL[]   = "RENAME TABLE dp23_cuspg TO dp3_cuspg;";