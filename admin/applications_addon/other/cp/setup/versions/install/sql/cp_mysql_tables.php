<?php

//-----------------------------------------------
// (DP32) Pages
//-----------------------------------------------
//-----------------------------------------------
// MySQL ALTER TABLES & ALTER FIELDS
//-----------------------------------------------
// Author: DawPi
// Site: http://www.ipslink.pl/
// Written on: 20 / 06 / 2010
//-----------------------------------------------
// Copyright (C) 2010 DawPi
// All Rights Reserved
//-----------------------------------------------  


$TABLE[] = "CREATE TABLE dp3_cuspg (
  cp_id int(10) NOT NULL AUTO_INCREMENT,
  cp_position tinyint(3) NOT NULL DEFAULT '1',
  cp_active tinyint(1) NOT NULL DEFAULT '0',
  cp_title varchar(255) NOT NULL,
  cp_page_desc text NOT NULL,  
  cp_seo_title varchar(255) NOT NULL,
  cp_text text NOT NULL,
  cp_groups varchar(250) NOT NULL DEFAULT '',
  cp_dateadded int(11) NOT NULL DEFAULT '0',
  cp_updated int(11) NOT NULL DEFAULT '0',
  cp_keywords text NOT NULL,
  cp_description text NOT NULL,
  cp_allow_html tinyint(1) NOT NULL DEFAULT '0',
  cp_allow_bbcode tinyint(1) NOT NULL DEFAULT '1', 
  cp_is_js tinyint(1) NOT NULL DEFAULT '0',
  cp_show_in_top tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (cp_id)	
)";