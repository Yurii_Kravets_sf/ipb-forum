<?php

//-----------------------------------------------
// (DP32) Pages
//-----------------------------------------------
//-----------------------------------------------
// Public Module
//-----------------------------------------------
// Author: DawPi
// Site: http://www.ipslink.pl
// Written on: 13 / 07 / 2009
// Updated on: 28 / 07 / 2011
//-----------------------------------------------
// Copyright (C) 2009 - 2011 DawPi
// All Rights Reserved
//-----------------------------------------------   


if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.";
	exit();
}

class public_cp_view_display extends ipsCommand
{
	/* Some inits */

    protected  $output;
              
	public function doExecute( ipsRegistry $registry )
	{	
		/* Set navigation */
		
		$this->registry->output->addNavigation( $this->settings['dp30custompages_title'], 'app=cp', 'false', 'cp' );

		/* Enabled? */
	    
	    if ( ! $this->settings['custompages_enable'] AND ! in_array( $this->memberData['member_group_id'], explode(',', $this->settings['dp30custompages_groups_exclude'] ) ) )
	    {
		   $this->registry->getClass('output')->showError( $this->lang->words['dp30custompages_error_disable'], 'DP31CP005' );
		}	    
	    
		/* Allowed group? */
		
	    if ( ! in_array( $this->memberData['member_group_id'], explode(',', $this->settings['dp30custompages_groups'] ) ) AND $this->settings['custompages_enable'] )
	    {
		   $this->registry->getClass('output')->showError( $this->lang->words['dp30custompages_error_group'], 'DP31CP006' );
		}
		
        /* Grab the parser file */
        
		$classToLoad = IPSLib::loadLibrary( IPS_ROOT_PATH . 'sources/classes/text/parser.php', 'classes_text_parser' );
		$this->parser = new $classToLoad();
        
		/* What we want to do? */

		switch( $this->request['do'] )
		{
			case 'show':
				$this->showPage();
			break;
				
			default:
				$this->showPages();
			break;
		}	
	}
	
	
	public function showPage()
	{
		/* INIT */
		
		$id = intval( $this->request['pageId'] );
		
		/* Check ID */
		
		if ( ! $id )
		{
		   $this->registry->getClass('output')->showError( $this->lang->words['dp30custompages_error_wrongid'], 'DP31CP001' );
		}
			
		/* Check page exist */
		
		$page = $this->DB->buildandFetch( array (
											'select'	=> '*',
											'from'		=> 'dp3_cuspg',
											'where'		=> 'cp_id = ' . $id
								   			) );
		
		if ( ! $page['cp_id'] )
		{
		   $this->registry->getClass('output')->showError( $this->lang->words['dp30custompages_error_noexist'], 'DP31CP002' );		
		}
			
		/* Check activity */
		
		if ( ! $page['cp_active'] )
		{
		   $this->registry->getClass('output')->showError( $this->lang->words['dp30custompages_error_noactive'], 'DP31CP003' );			
		}
			
		/* Check group permission to this page */
		
		if ( ! in_array( $this->memberData['member_group_id'], explode(',', $page['cp_groups'] ) ) )
		{
		   $this->registry->getClass('output')->showError( $this->lang->words['dp30custompages_error_noperm'], 'DP31CP004' );		
		}
		
		/* Still here? Show page! */
		
	    $page['cp_dateadded'] = $this->registry->getClass( 'class_localization')->getDate( $page['cp_dateadded'], 'LONG', 1 );
	    $page['cp_updated']   = $this->registry->getClass( 'class_localization')->getDate( $page['cp_updated'], 'LONG', 1 );
				
        if( ! $page['cp_is_js'] )
		{
    		/* Set up parser */
            
    		$this->parser->set( array( 'memberData'     => $this->memberData,
    								   'parseBBCode'    => 1,
    								   'parseArea'      => 'cp_view',
    								   'parseHtml'      => 0,
    								   'parseEmoticons' => 1 ) );

    		/* Make sure we have the display look */
            
    		$page['cp_text'] = $this->parser->display( $page['cp_text'] );
	    }
	    else
	    {
			/* Fix for .js codes */
			
			$page['cp_text'] = str_replace( '&#092;', '\\', $page['cp_text'] );			
		}
	    
		/* Set page title */
		 
		$this->title = $page['cp_title'];
		
		/* Do we have keywords? */
		
		if( IPSText::mbstrlen( $page['cp_keywords'] ) )
		{
			$this->registry->output->addMetaTag( 'keywords', $page['cp_keywords'] );
		}

		/* Do we have description? */
		
		if( IPSText::mbstrlen( $page['cp_description'] ) )
		{
			$this->registry->output->addMetaTag( 'description', $page['cp_description'] );
		}
		
		/* Set navigation */
						
		$this->registry->output->addNavigation( $page['cp_title'], '' );
		
		/* Show! */
		
		$this->output = $this->registry->output->getTemplate('cp')->showPage( $page );
				
		$this->makeHtml();	
	}
	
	public function showPages()
	{				
		/* Can we display pages list? */
		
		if( ! $this->settings['dp3_cp_read_all'] )
		{
		   $this->registry->getClass('output')->showError( $this->lang->words['dp30custompages_error_nodirectly'], 'DP31CP007' );				
		}
			
		/* Pagination */
				
		$start = intval( $this->request['st'] ) >= 0 ? intval( $this->request['st'] ) : 0;   
				
		/* Make query */
		
		$this->DB->build( array(
							'select'	=> '*',
							'from'		=> 'dp3_cuspg',
							'where'		=> 'cp_active = 1',
							'order'     => 'cp_position',		
							) );
		
		$this->DB->execute();
		
		$i = 0;
		
		if ( $this->DB->getTotalRows() )
		{
		   while ( $page = $this->DB->fetch() ) 
		   {
			   if ( in_array($this->memberData['member_group_id'], explode(',',$page['cp_groups'] ) ) )
			   {
			   	   $i++;
			   }		   
		   }
        }
		 			 
       $pagination = $this->registry->output->generatePagination( array( 
																		'totalItems'		=> $i,
																		'itemsPerPage'		=> $this->settings['dp30custompages_items'],
																		'currentStartValue'	=> $start,
																		'baseUrl'			=> "app=cp",
																	)
															); 

		/* Make query */
		
		$this->DB->build( array(
							'select'	=> '*',
							'from'		=> 'dp3_cuspg',
							'where'		=> 'cp_active = 1',
							'order'     => 'cp_position',	
							'limit'     => array( $start, $this->settings['dp30custompages_items'] ),								
							) );
		
		$this->DB->execute();
		
		if ( $this->DB->getTotalRows() )
		{
		   while ( $page = $this->DB->fetch() ) 
		   {
		   
			   if ( in_array( $this->memberData['member_group_id'], explode(',', $page['cp_groups'] ) ) )
			   {			   
				   $page['cp_dateadded'] = $this->registry->getClass( 'class_localization')->getDate( $page['cp_dateadded'], 'LONG', 1 );
				   $page['cp_updated']   = $this->registry->getClass( 'class_localization')->getDate( $page['cp_updated'], 'LONG', 1 );
				   
				   $page['cp_title_seo'] = IPSText::makeSeoTitle( $page['cp_title'] );
						   
				   $pages[] = $page;
			   }	   
		   }		
		}
		
		else
		{
		 	$pages = array();		
		}					
					 
		/* Show me! O.o */
		
		$this->output = $this->registry->output->getTemplate('cp')->showAllPages( $pages, $pagination );
				
		$this->makeHtml();
	}
	
	
	private function makeHtml()
	{
		/* Send to screen */
		
        $this->output .= $this->registry->cpLibrary->small_c_acp();
        
		$this->registry->output->addContent( $this->output );
				
		$this->registry->output->setTitle( $this->settings['dp30custompages_title'] . ( strlen( $this->title ) ? ' - ' . $this->title : '' ) );
		
		$this->registry->output->sendOutput();	
	}
} // End of class