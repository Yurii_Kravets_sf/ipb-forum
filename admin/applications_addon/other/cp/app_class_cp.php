<?php

//-----------------------------------------------
// (DP32) Pages
//-----------------------------------------------
//-----------------------------------------------
// Loader
//-----------------------------------------------
// Author: DawPi
// Site: http://www.ipslink.pl
// Written on: 27 / 07 / 2011
//-----------------------------------------------
// Copyright (C) 2011 DawPi
// All Rights Reserved
//-----------------------------------------------   

class app_class_cp
{
	public function __construct( ipsRegistry $registry )
	{
		$classToLoad = IPSLib::loadLibrary( IPSLib::getAppDir( 'cp' ) . '/sources/classes/library.php', 'cpLibrary', 'cp' );
		$registry->setClass( 'cpLibrary', new $classToLoad( $registry ) );			
		
		if ( IN_ACP )
		{			
			$registry->getClass('class_localization')->loadLanguageFile( array( 'admin_cp' ),  'cp' );
		}
		else
		{
			$registry->getClass('class_localization')->loadLanguageFile( array( 'public_cp' ), 'cp' );
		}
	}		
}// End of class