<?php

//-----------------------------------------------
// (DP33) Pages
//-----------------------------------------------
//-----------------------------------------------
// Application Library
//-----------------------------------------------
// Author: DawPi
// Site: http://www.ipslink.pl
// Written on: 27 / 07 / 2011
//-----------------------------------------------
// Copyright (C) 2011-2012 DawPi
// All Rights Reserved
//-----------------------------------------------  

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.";
	exit();
}

class cpLibrary
{
	protected $registry;

	
	public function __construct( ipsRegistry $registry )
	{		
		/* Make registry objects */
		$this->registry   =  $registry;
		$this->DB         =  $this->registry->DB();
		$this->settings   =& $this->registry->fetchSettings();
		$this->request    =& $this->registry->fetchRequest();
		$this->lang       =  $this->registry->getClass('class_localization');
		$this->member     =  $this->registry->member();
		$this->memberData =& $this->registry->member()->fetchMemberData();
		$this->cache      =  $this->registry->cache();
		$this->caches     =& $this->registry->cache()->fetchCaches();		
	}

	
	public function checkAccess()
	{
		/* Application is enabled? */
		
		if( ! $this->caches['app_cache']['cp']['app_enabled'] )
		{
			return false;
		}		
			
		/* System is enabled */
		
		if( ! $this->settings['custompages_enable'] )
		{
			return false;
		}
		
		/* Allowed group? */
		
		if( ! in_array( $this->memberData['member_group_id'], explode(',', IPSText::cleanPermString( $this->settings['dp30custompages_groups'] ) ) ) )
		{
			return false;
		}

		/* Still here? */
		
		return true;		
	}
	    
	public function updateCache()
	{
		/* INIT */
		
		$cache = array();
		
		/* Get content from DB */
						 
		$this->DB->build( array(
							'select'	=> '*',
							'from'		=> 'dp3_cuspg',
							'order'		=> 'cp_position ASC'																													
		)	);							 
									 
		/* Execute */
		
		$query = $this->DB->execute();
		
		/* Parse */
		
		if ( $this->DB->getTotalRows( $query ) )
		{			
			while ( $row = $this->DB->fetch( $query ) )
			{
				$cache[ $row['cp_id'] ] = $row;
			}
		}
		
		/* Update cache */

		$this->cache->setCache( 'cp_pages', $cache, array( 'array' => 1, 'donow' => 0, 'deletefirst' => 1 ) );		
	}
	
	/**
	 * Add Copyright Statement
	 *
	 * @access	public
	 * @author	DawPi
	 * @return	string	Processed HTML
	 */

	public function c_acp()
	{
		return "<div id='footer' style='margin: 0px; text-align: center;-moz-border-radius:5px;'>Powered By <strong>" . IPSLib::getAppTitle( 'cp' ) . " " . $this->caches['app_cache']['cp']['app_version'] . "</strong> &copy; 2009" . ( '2009' != date( 'Y' ) ?  '-' . date( 'Y' ) : '' ) . " &nbsp;<a target='_blank' href='http://www.ipslink.pl/' title='Custom mods, skins, support and more. We develop for you.'>IPSlink.pl</a></div>";
	}
	
	public function small_c_acp()
	{
		return "<div style='margin-top: 10px; float: right; color: #606060; font-size: 0.8em;'>Powered By " . IPSLib::getAppTitle( 'cp' ) . " " . $this->caches['app_cache']['cp']['app_version'] . " &copy; <a target='_blank' href='http://www.ipslink.pl/' title='Custom mods, skins, support and more. We develop for you.'>IPSlink.pl</a></div>";
	}
} // End of class