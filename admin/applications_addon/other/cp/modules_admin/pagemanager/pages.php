<?php

//-----------------------------------------------
// (DP34) Pages
//-----------------------------------------------
//-----------------------------------------------
// Application
//-----------------------------------------------
// Author: DawPi
// Site: http://www.ipslink.pl
// Written on: 11 / 07 / 2009
// Updated on: 17 / 12 / 2012
//-----------------------------------------------
// Copyright (C) 2009 - 2012 DawPi
// All Rights Reserved
//-----------------------------------------------   

class admin_cp_pagemanager_pages extends ipsCommand
{
	public $html;
    
	private $parser;
	private $editor;
    
	public function doExecute( ipsRegistry $registry )
	{
		/* Load skin and lang */
		
		$this->html               = $this->registry->output->loadTemplate( 'cp_cp_pages' );		
		$this->html->form_code    = 'module=pagemanager&amp;section=pages';
		$this->html->form_code_js = 'module=pagemanager&section=pages';	

		/* Check acp restriction */
		
		$this->registry->getClass('class_permissions')->checkPermissionAutoMsg( 'manage' );
		
		/* Load the editor */
        
		$classToLoad = IPSLib::loadLibrary( IPS_ROOT_PATH . 'sources/classes/editor/composite.php', 'classes_editor_composite' );
		$this->editor = new $classToLoad();
		
		/* Turn off legacy mode */
        
		$this->editor->setLegacyMode( false );
                
		/* What we want to do? */
		
		switch ( $this->request['do'] )
		{
			case 'addPage':		
				$this->form( 'add' );
			break;
			
			case 'editPage':		
				$this->form( 'edit' );
			break;
							
			case 'doEditPage':		
				$this->doForm( 'edit' );
			break;			
				
			case 'checkPage':		
				$this->checkPage();
			break;	
			
			case 'togglePage':		
				$this->doTogglePage();
			break;
			
			case 'reorder':		
				$this->doReorderPage();
			break;	
										
			case 'removePage':		
				$this->removePage();
			break;	
											
			case 'removeAll':		
				$this->removeAllPages();
			break;	
			
			case 'updateCache':
				$this->doUpdateCache();
			break;
						
			case 'view':							
			default:
				$this->viewMain();
			break;
		}
		
		$this->registry->output->html_main .= $this->registry->output->global_template->global_frame_wrapper();

		$this->registry->output->html .= $this->registry->cpLibrary->c_acp();
		
		$this->registry->output->sendOutput();
	}
	
		
	public function viewMain()
	{		   		
		/* Build pagination */
			
		$start 		= intval( $this->request['st'] ) >= 0 ? intval( $this->request['st'] ) : 0;   
				
		$count 		= $this->DB->buildAndFetch( array( 'select'   => 'count(*) as cnt', 'from'     => 'dp3_cuspg' ) );
						 					 
        $pagination = $this->registry->output->generatePagination( array( 
																		'totalItems'		=> $count['cnt'],
																		'itemsPerPage'		=> $this->settings['dp30custompages_items'],
																		'currentStartValue'	=> $start,
																		'baseUrl'			=> $this->settings['base_url'] . "&" . $this->html->form_code,
																	)
															);       

		/* Get pages from DB */
		
		$this->DB->build( array( 'select'   => '*',
								 'from'     => 'dp3_cuspg',
								 'order'    => 'cp_position ASC',
								 'limit'    => array( $start, $this->settings['dp30custompages_items'] ),								 
						 )		);
		
		$this->DB->execute();
		
		/* Parse pages */
		
		if ( $this->DB->getTotalRows() )
		{
			while ( $row = $this->DB->fetch() )
			{
				/* Store it! :D */
				
				$pages[] = $row;			
			}
		}
			
		/* Add to output */
		
		$this->registry->output->html .= $this->html->pagesListView( $pages, $pagination );				
	}
	
	
	public function removePage()
	{
		$id = intval( $this->request['page_id'] );
		
		if ( ! $id )
		{
			$this->registry->output->showError( $this->lang->words['errorNoId'], 'DP3_CP_001' );		
		}
		
		/* Delete it :( */
		
		$this->DB->delete( 'dp3_cuspg', 'cp_id = ' . $id );
		
		/* Update cache */
			
		$this->cache->rebuildCache( 'cp_pages', 'cp' );
						
		/* Save log and redirect */
		
		$this->registry->getClass('adminFunctions')->saveAdminLog( $this->lang->words['adminLogSuccessDelete'] . $id );
		
		$this->registry->output->redirect( $this->settings['base_url'].$this->html->form_code . "&amp;st=" . $this->request['st'], $this->lang->words['adminSuccessDelete'] . $id );
	}
	
	
	public function removeAllPages()
	{
		/*Delete */
		
		$this->DB->delete( 'dp3_cuspg' );
		
		/* Update cache */
			
		$this->cache->rebuildCache( 'cp_pages', 'cp' );
				
		/* Save log and redirect */
		
		$this->registry->getClass('adminFunctions')->saveAdminLog( $this->lang->words['adminSuccessDeleteAll'] );
		
		$this->registry->output->redirect( $this->settings['base_url'].$this->html->form_code, $this->lang->words['adminSuccessDeleteAll'] );			
	}
	
	
	public function form( $type = 'add' )
	{
 		 /* INIT */
 		 
		 $formData = array();
 		 $st	   = $this->request['st'];
 		 
 		 /* Build button */
 		 
         $button   = ( $type == 'add' ) ? $this->lang->words['add'] : $this->lang->words['edit'] ;

		/* Navigation */

		$this->registry->output->extra_nav[] = array( '', $button . $this->lang->words['page'] );

		/* Get group cache */

		foreach( $this->cache->getCache('group_cache') as $g )
		{
			$allGroups[] = array( $g['g_id'], $g['g_title'] );
		}
		
		/* Wha we're doing? */
				
		if ( $type == 'add' )
		{
			$button             	= $this->lang->words['add'] . $this->lang->words['page'];
			$typeAfter          	= 'add';	
			$formData['formDo'] 	=  $this->lang->words['addNewPageDo']; 
			$formData['formDoDesc'] =  $this->lang->words['addNewPageDoDesc'];  			 
		}
		
		else if ( $type = 'edit' )
		{
			$button                 = $this->lang->words['savepage'];
			$typeAfter          	= 'edit';
			$formData['formDo'] 	=  $this->lang->words['editPageDo'];		  			
			$formData['formDoDesc'] =  $this->lang->words['editPageDoDesc']; 
			
			/* Get id */
			 				
			$id = intval( $this->request['page_id'] );
			
			if ( ! $id )
			{
			   $this->registry->output->showError( $this->lang->words['errorNoId'], 'DP3_CP_002' );			
			}
			
			/* Get data from SQL */
			
			$data = $this->DB->buildAndFetch( array( 'select' => '*', 'from'	 => 'dp3_cuspg', 'where'	 => 'cp_id = '. $id ) );
							
			/* Do we have this page in DB? */
			
			if ( ! $this->DB->getTotalRows() )
			{
			   $this->registry->output->showError( $this->lang->words['errorNoPage'], 'DP3_CP_003' );				
			}															  		
		}
					
		/* Sort form values */

		$formData['cp_id']     			= $data['cp_id'];
		$formData['cp_title']  			= $this->registry->output->formInput( 'cp_title', $data['cp_title'] );
		$formData['cp_page_desc']    	= $this->registry->output->formTextarea( 'cp_page_desc', $data['cp_page_desc'], 55, 2 );		
		$formData['cp_active'] 			= $this->registry->output->formYesNo( 'cp_active', $data['cp_active'] );
		$formData['cp_groups'] 			= $this->registry->output->formMultiDropdown( "cp_groups[]", $allGroups, explode( ",", $data['cp_groups'] ), 6 );
		$this->editor->setContent( $data['cp_text'] );
        $formData['cp_text']   			= $this->editor->show( 'cp_text', array( 'delayInit' => 1 ) ); 
		$formData['cp_keywords']  		= $this->registry->output->formInput( 'cp_keywords', $data['cp_keywords'] );
		$formData['cp_description']  	= $this->registry->output->formTextarea( 'cp_description', $data['cp_description'], 55, 3 );
		$formData['cp_allow_html'] 		= $this->registry->output->formYesNo( 'cp_allow_html', $data['cp_allow_html'] );
		$formData['cp_allow_bbcode']	= $this->registry->output->formYesNo( 'cp_allow_bbcode', $data['cp_allow_bbcode'] );				
		$formData['cp_is_js']			= $this->registry->output->formYesNo( 'cp_is_js', $data['cp_is_js'] );	
		$formData['cp_show_in_top']		= $this->registry->output->formYesNo( 'cp_show_in_top', $data['cp_show_in_top'] );
		
		/* Add to output */
		
		$this->registry->output->html .= $this->html->formView( $formData, $button, $typeAfter, $st );					
	}
	
	
	public function checkPage()
	{
		/* No title? Error! */   
		
		if ( ! IPSText::mbstrlen( $this->request['cp_title'] ) ) 
		{ 
			$this->registry->output->showError( $this->lang->words['errorNoTitle'], 'DP3_CP_004' );
		}

		/* No page body? */
	
		if ( ! IPSText::mbstrlen( $this->request['cp_text'] ) )
		{
		    $this->registry->output->showError( $this->lang->words['errorNoTxt'], 'DP3_CP_005' );
		} 
         
		/* Set content in editor */
        
		$this->editor->setAllowBbcode( true );
		$this->editor->setAllowSmilies( true );
		$this->editor->setIsHtml( 0 );
        
		/* Play with content */
		
		if( ! $this->request['cp_is_js'] )
		{								
			$postContent = $this->editor->process( $_POST['cp_text'] );
	    }
	    else
	    {
			$postContent = $_POST['cp_text'];		
		}
	   
		/* Build array with data */ 
		
		$saveArray =  array(
							'cp_title'			=> $this->request['cp_title'],
							'cp_page_desc'		=> $this->request['cp_page_desc'],							
							'cp_seo_title'		=> IPSText::makeSeoTitle( $this->request['cp_title'] ),
							'cp_active'			=> $this->request['cp_active'],
							'cp_groups'			=> ( count( $this->request['cp_groups'] ) ) ? ( implode( ',', $this->request['cp_groups'] ) ) : '0',
							'cp_text'			=> $postContent,
							'cp_keywords'		=> trim( IPSText::getTextClass('bbcode')->stripBadWords( IPSText::stripAttachTag( $this->request['cp_keywords'] ) ) ),
							'cp_description'	=> trim( IPSText::getTextClass('bbcode')->stripBadWords( IPSText::stripAttachTag( $this->request['cp_description'] ) ) ),
							'cp_allow_bbcode'	=> 1,
							'cp_allow_html'		=> 0,
							'cp_is_js'			=> intval( $this->request['cp_is_js'] ),
							'cp_show_in_top'	=> intval( $this->request['cp_show_in_top'] ),
						    );
	
		
		if ( $this->request['type'] == 'add' )
		{
			$saveArray['cp_dateadded']  = time();
			
			/* Insert! */
			
			$this->DB->insert( 'dp3_cuspg', $saveArray );    	     	   
			
			/* Update cache */
				
			$this->cache->rebuildCache( 'cp_pages', 'cp' );
		
			/* Redirect */
			
			$this->registry->output->redirect( $this->settings['base_url'].$this->html->form_code. "&amp;st=" . $this->request['st'], $this->lang->words['adminSuccessAdd'] );		
		}
		
        else if ( $this->request['type'] == 'edit' )
        {
			/* Set message */
			
			$this->registry->output->global_message = $this->lang->words['adminSuccessEdit'];
			
			$saveArray['cp_updated']  = time();
			
			$this->DB->update( 'dp3_cuspg', $saveArray, 'cp_id=' . $this->request['page_id'] );       
		
			/* Update cache */
				
			$this->cache->rebuildCache( 'cp_pages', 'cp' );
					
			/* Reload? */
			
			if( isset( $this->request['save_and_reload'] ) )
			{	
				/* Redirect */
				
				$this->registry->output->silentRedirectWithMessage( $this->settings['base_url'] . $this->form_code  . '&amp;do=editPage&amp;page_id=' . $this->request['page_id'] );	
			}
			
			/* Redirect */
			
			$this->registry->output->silentRedirectWithMessage( $this->settings['base_url'] . $this->html->form_code . '&amp;st=' . $this->request['st'] );		
		}	
	}
	
	public function doReorderPage()
	{ 		
 		/* Get ajax class */
		
		$classToLoad = IPSLib::loadLibrary( IPS_KERNEL_PATH . 'classAjax.php', 'classAjax' );
		$ajax = new $classToLoad();		
		
		/* Checks... */

		if( $this->registry->adminFunctions->checkSecurityKey( $this->request['md5check'], true ) === false )
		{
			$ajax->returnString( $this->lang->words['postform_badmd5'] );
			exit();
		}		
			
		/* Save new position */

 		$position = 1 + intval( $this->request['st'] );

 		if( is_array( $this->request['pages'] ) AND count($this->request['pages'] ) )
 		{
		 	foreach( $this->request['pages'] as $this_id )
 			{
 				$this->DB->update( 'dp3_cuspg', array( 'cp_position' => $position ), 'cp_id = ' . $this_id );
 				
 				$position++;
 			}
 		}
 		
		/* Update cache */
			
		$this->cache->rebuildCache( 'cp_pages', 'cp' );
		
		/* A ok */
 		
		$ajax->returnString( 'OK' );
 		
		exit();			
	}
	
	
	public function doTogglePage()
	{
		$id = intval( $this->request['page_id'] );
		
		$act = $this->DB->buildAndFetch( array( 'select' => 'cp_id, cp_active', 'from' => 'dp3_cuspg', 'where' => 'cp_id = ' . $id ) );
		
		if ( ! $this->DB->getTotalRows())
		{
			 $this->registry->output->showError( $this->lang->words['errorNoPage'], 'DP3_CP_006' );
		}
		
		$sett = ( $act['cp_active'] ) ? 0 : 1 ;	
		
		$this->DB->update( 'dp3_cuspg', array('cp_active' => $sett ) , 'cp_id = ' . $id );
 		
		 /* Update cache */
			
		$this->cache->rebuildCache( 'cp_pages', 'cp' );
		
		/* Set message */
			
		$this->registry->output->global_message = $this->lang->words['successtoggled'];
					 
		/* Redirect */	  
		
		$this->registry->output->silentRedirectWithMessage( $this->settings['base_url'] . $this->html->form_code  . "&amp;st=" . $this->request['st']  );	
	}
	
	public function doUpdateCache()
	{
		/* Update cache */
			
		$this->cache->rebuildCache( 'cp_pages', 'cp' );
			
		/* Set message */
		
		$this->registry->output->global_message = $this->lang->words['success_cache_updated'];	
						
		/* Redirect */	
		  
		$this->registry->output->silentRedirectWithMessage( $this->settings['base_url'] . $this->html->form_code );				
	}
		
}// End of class